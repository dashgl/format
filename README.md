# Dash Model Format

![Dashie](https://gitlab.com/dashgl/format/-/raw/master/mascot/dashgl_banner.png)

_Version 1.5_

The Dash Model Format (.dmf) is a simple-as-possible interchange format. The format is intended for individual rigged assets with animations. The intent of the format is to have an entire assets contained in a single file in a common format that can be reliably used with several different 3D content and creation tools. 

Dash Model Format 3D Formats Working Group

- Benjamin Collins [@kion_dgl](https://twitter.com/kion_dgl)  
- Andreas Scholl [@andreasscholl09](https://twitter.com/andreasscholl09)   

Copyright (C) 2019- 2020 DashGL Project


## Import Support

| Application  | Import Mesh  | Textures | Materials | Bones | Animations |
|---|---|---|---|---|---|
| [Threejs](https://gitlab.com/dashgl/format/-/tree/master/plugin/threejs) |  ✓ | ✓ | ✓ | ✓ | ✓ | 
| [Unity](https://gitlab.com/dashgl/unity) | ✓ | ✓ | ✓ | ✓ | ✓ | 
| [Noesis](https://gitlab.com/dashgl/format/-/tree/master/plugin/noesis)  | ✓ | ✓ | ✓ | ✓ | ✓ | 
| [Blender](https://gitlab.com/dashgl/format/-/tree/master/plugin/blender) | ✓ | ✓ | ✓ |  |  | 
| [FBX](https://gitlab.com/dashgl/format/-/tree/master/plugin/fbx) | ✓ | ✓ | ✓ | ✓ |  | 


## Export Support

| Application  | Export Mesh  | Textures | Materials | Bones  |  Animations  |
|---|---|---|---|---|---|
| [Threejs](https://gitlab.com/dashgl/format/-/tree/master/plugin/threejs) |  ✓ | ✓ | ✓ | ✓ | ✓ | 
| [Noesis](https://gitlab.com/dashgl/format/-/tree/master/plugin/noesis)  |  ✓ | ✓ |✓ | ✓ | ✓ | 


# Contents

* [1-0 Introduction](#1-0-introduction)
  * [1-1 Motivation](#1-1-motivation)
  * [1-2 Dash Basics](#1-2-dash-basics)
  * [1-3 Design Goals](#1-3-design-goals)
  * [1-4 Versioning](#1-4-versioning)
  * [1-5 File Extensions and MIME Types](#1-4-file-extensions)
  * [1-6 JSON Encoding](#1-6-json-encoding)
  * [1-7 Resources](#1-7-resources)
* [2-0 Concepts](#2-0-concepts)
  * [2-1 Attribute Table](#2-1-attribute-table) 
  * [2-2 Textures](#2-2-textures)
  * [2-3 Materials](#2-3-materials)  
  * [2-4 Vertices](#2-4-vertices)  
  * [2-5 Faces](#2-5-Faces)  
  * [2-6 Skeleton](#2-6-skeleton)
  * [2-7 Animations](#2-7-animations)


# 1-0 Introduction

![Dash Position](https://gitlab.com/dashgl/format/-/raw/master/mascot/dash_position.png)

The Dash Model Format (.dmf) is a simple-as-possible interchange format. The format is intended for individual rigged assets with animations. The intent of the format is to have an entire assets contained in a single file in a common format that can be reliably used with several different 3D content and creation tools. 

## 1-1 Motivation

![Dash Map](https://gitlab.com/dashgl/format/-/raw/master/mascot/dmf_map.png)

When I first started working with classic formats, I thought that reverse engineering the formats would be the end of the journey and that there would be a widely supported modern format that I could port to. At first I used the Threejs json format, as it was the easiest to work with, but it was quickly depreciated after I started working with it. After that I tried to write an exporter to .dae, but found the xml format quite difficult to work with and over confusing. Gltf finally came out, and after trying to work with it, I found it too complicated for my use case. By this time I've been messing around with the API's from Threejs, Blender and Noesis trying to support common formats, when I finally came to the realization that I could define my own format that I could reliably write plugins for different applications to be able to use that format directly, as opposed to trying to write to a common format and then watch it break, when actually trying to use it.

## 1-2 Dash Basics

![Dash Basics](https://gitlab.com/dashgl/format/-/raw/master/mascot/dash_basics.png)

The Dash Model Format is a binary format built around fixed-length structures to create a model that is easy to parse in any language. The structure of the file has a magic number at the top, a table that defines 6 attributes (textures, materials, vertices, faces, bones, and animations). Each attribute in the table provides a count and an offset to a section in the file. 

## 1-3 Design Goals

The Dash Model format is based around a single asset. The figure above provides some contrast, simplier models only define meshes with textures but no bones or animations. More complicated formats with define entire scenes with rigged assets, lights and cameras. The Dash Model Format is intended to be a format that defines a single rigged asset. The way the file defines a single assets is intended to target a specific "Dreamcast-style" use-case, with skeletal rigging (no morph targets), and a single diffuse uv channel. The idea is to create a specific use-case to work with that all 3D modeling tools can realiably work with and target that. If any requests or required changes are needed to be implemented they will be planned and implemented in future versions.

## 1-4 Versioning

The intended version scheme was to have stable releases be 1.0, 2.0, ect. But the original 1.0 release under-went changes and the first "stable" release is 1.5. So all tools will be built around targetting that. Future changes will be planned and the level of version incrementation will be based around the implementation plan. It is itended for subversions (2.1, 2.23, ect) to be developer versions, and full versions (2.0, 3.0) to be end-user-friendly releases, but we'll see how well that works out. 

## 1-5 File Extensions

The dash model format comes in two formats, the binary format and a companion json format. The intended distribution format is the binary format, but the tools will be designed to parse the binary format into the json format before being read into the application. And like-wise at export time, create a json represention before converting to binary so that the user can choose which format they would like to write to. Both formats will have the extension of .dmf. The binary version will have the magic number 'DASH' at the beginning, and if that is not found, the JSON format will be assumed. 

## 1-6 Json Encoding

JSON encoding must use ASCII and fit inside the constraints defined in the binary version of the file format. (Name strings must fit to 32 characters)

## 1-7 Resources

- [Online viewer](https://dashgl.gitlab.io/format/preview/)  
- [Atlas Creation](https://dashgl.gitlab.io/format/atlas/)
- [File Format Wiki](https://gitlab.com/dashgl/format/wikis/home)
- [Sample Unity Project](https://gitlab.com/dashgl/unity)

# 2-0 Concepts

**Binary**

The binary version of the Dash Model Format is intended as the starndard form. The reason for this is to provide a format that can be easily parsed by various 3D applications, tools and engines. The start of the binary file is given by the following 0x10 byte struct. 

```
typedef struct {
	uint32_t magic;
	uint16_t major_version;
	uint16_t minor_version;
	uint32_t header_offset;
	uint32_t header_count;
} DashMagic_t;
```

The first four bytes of the file are defined by the magic number 'DASH' or 0x48534144 in little endian. Following that is a 2 byte short value for the major version, and then a 2 byte short value for the minor version. As stated in the version section of this document, minor version changes are intended for development, with full version changes intended for stable releases. 

The next 4 bytes in the header is the offset to the attribute table which defines the geometry of the asset. In the current version this will always be at offset 0x10. Originally it was intended for space to be allowed for meta data to be inserted into the top of the file if needed, but a META data attribute will likely be included in future definitions. The last four bytes in the header are for the count of attributes in the attribute table. This is redundant and should always be 6 for the current 1.5 version, and will likely be increased to 7 when the meta attribute is introduces.

**JSON**

For the JSON version, a single file defines a single asset, so the file format is defined by a single JSON object.

```
{
  "format" : "DASH",
  "version" : "1.5"
  ...
}
```

In place of the magic number, there should be a key "format" with the string "DASH" in the top level of the object to define that the JSON object is intended to be interpretted as a DASH JSON file. A key "version" should also be included so that the importer can accurately recognize the struct values that need to be defined for each attribute.

# 2-1 Attribute Table

**Binary**

The main contents of the geometry are all defined by the attribute table at the top of the file. This is included to act as a table of contents to try and provide as much information to the developer as possible. The binary file format should be well documented, but steps should taken to make the file format as intuitive and easy as possible to reverse engineer to make it as simple and flexible as possible to work with.

| Offset | 0x00 | 0x04 | 0x08 | 0x0c |
| ------ | ------ | ------ | ------ | ------ |
| 0x0000 | DASH | major(1) / minor(5) version | Attr Table Offset (0x10) |  Attr Count (6) | 
| 0x0010 | TEX | flags(0) | Texture List Offset |  Texture Count | 
| 0x0020 | MAT | flags(0) | Material List Offset |  Material Count | 
| 0x0030 | VERT | flags(0) | Vertex List Offset |  Vertex Count | 
| 0x0040 | FACE | flags(0) | Face List Offset |  Face Count | 
| 0x0050 | BONE | flags(0) | Bone List Offset |  Bone Count | 
| 0x0060 | ANIM | flags(0) | Anim List Offset |  Anim Count | 

The table above shows the header for the binary version of the file format. The first 0x10 bytes show the Magic number structure that is defined in the previous section of this document. Following that is an attribute table for the following attributes, "Textures", "Materials", "Vertices", "Faces", "Bones" and "Animations". For the table the first four bytes is a magic number table to make it easier to read and trace from looking at the file. The four bytes after that are reserved for flags, however these are likely not required and act as padding so that each attribute struct to have a length of 0x10 bytes. The next four bytes are the offset to the defined attribute list, and the last four bytes is the length of the structs defined in the list.

```
#define DASH_LABEL_TEX 	0x00786574
#define DASH_LABEL_MAT 	0x0074616D
#define DASH_LABEL_VERT 0x74726576
#define DASH_LABEL_FACE 0x65636166
#define DASH_LABEL_BONE 0x656E6F62
#define DASH_LABEL_ANIM 0x6D696E61

typedef struct {
	uint32_t label;
	uint32_t flags;
	uint32_t offset;
	uint32_t count;
} DashAttribute_t;

typedef struct {
	DashAttribute_t tex;
	DashAttribute_t mat;
	DashAttribute_t vert;
	DashAttribute_t face;
	DashAttribute_t bone;
	DashAttribute_t anim;
} DashAttrTable_t;
```

**JSON**

```
{
  "format" : "DASH",
  "version" : "1.5"
  "textures" : [ ... ],
  "materials" : [ ... ],
  "verterices" : [ ... ],
  "faces" : [ ... ],
  "skeleton" : [ ... ],
  "animations" : [ ... ]
}
```

For the JSON version, seeking isn't required, since attributes can be accessed by a key. And lengh is also provided with the array structure, so the attribute list is provided by top level keys which all define arrays. The keys are expected to be "textures", "materials", "verterices", "faces", "skeleton", "animations". "Meta" is another attribute which will likely be defined in new releases. 

## 2-2 Textures

**Binary**

For the binary version, the texture structure is defined as follows. 

```
typedef struct {
	char name[0x20];
	int16_t tex_id;
	uint16_t flip_y;
	uint32_t img_offset;
	uint32_t img_length;
	uint16_t img_width;
	uint16_t img_height;
	uint16_t wrap_s;
	uint16_t wrap_t;
	uint8_t nop[0x0c];
} DashTexture_t;
```

The struct starts with a name for the texture which is limited to 32 characters. Following that is the texture id, which should start at 0 for the first item in the list and increment by one after that. This attribute is generally for show as the position in the array takes priority over this value. As materials with a texture id reference will reference the position in the array, this attribute is included to make the binary file easier to trace through. Following that is binary true/false value for Flip Y. I can never keep track of which one is which so I'll probably have to make a table for which application does what to keep these consistent. 

The image offset is the offset inside the file where the image data can be found. And the image length is the length of the binary for the internal image file. Image files are always in PNG. It might be possible to use allow for different image formats such as DDS, JPEG, or TGA, but the image will need to be reliably parsed or written to from a vareity of applications. So for now it's thought that PNG offers the widest standard support across multiple lanaguages and multiple applications, and offers vaious coding and file compression. The image width and image height are also included. Followed by the settings for wrap S and T overfow settings, specifically clamp, repeat or mirror. 

The last 0x0c bytes of the file are used for padding to keep the struct length at 0x40. This could be used for filter settings, center, offset, rotation or mipmaps. But these are not currently supported and have been left out intentionally to avoid over-complication. They could be added into the file format definition if deemed neccessary or useful. 

**JSON**

```
{
  "id" : int,
  "name" : string,
  "flip_y" : bool,
  "img" : "data:image/png;base64,...",
  "width" : int,
  "height" : int,
  "wrap_s" : enum("clamp","mirror","repeat"),
  "wrap_t" : enum("clamp","mirror","repeat")
}
```

For the JSON version, the id is a whole number, the name is a string in quotes (suggested limited to 32 characters), flip_y is a 0 or 1 boolean value. The image data is included as a base64 data string. The width and height are included as numbers, and wrap_s and wrap_t are enums, meaning they should have a specific string value, or an int that matches the index of the intended value.

# 2-3 Materials

**Binary**

The Dash Model format approach to materials is to define everything. This is in contrast to a lot of other binary formats that I've dealt with that will often use bitflags at the start of the struct in order to indicate which properties either are, or aren't defined in the struct, and have a variable struct length. So to get around this, the Dash Model Format used a fixed struct where all of the properties for a material are defined, and default values are used, or properties that aren't needed can be ignored.

```
typedef struct {
	char name[0x20];
	int16_t mat_id;
	int16_t tex_id;
	uint16_t use_blending;
	uint16_t blend_equation;
	uint16_t blend_src;
	uint16_t blend_dst;
	uint16_t skinning;
	uint16_t shader_type;
	uint16_t face_side;
	uint16_t shadow_side;
	uint16_t ignore_lights;
	uint16_t vertex_color;
	uint16_t use_alpha;
	uint16_t skip_render;
	float alpha_test;
	float diffuse[4];
	float emissive[4];
	float specular[4];
} DashMaterial_t;
```

The first 32 bytes of the struct is provided for the material name. I'm not sure how often materials are actually named in practice, but a name like "material_000", or "material_001", is suggested if none is already defined as it futher helps with tracing through the file. Following the name is the id, this is a 2 byte short value that is expected to be the index of the material. Note that similar to textures, the array position in the file takes priority, so in a situation where the id is out of order, the material_id property in the face struct will use the index and not the id. Following the material id is the texture id. This will be the diffuse mapping for the material. If there is a texture it should use an id from 0-n depending on the number, and if no texture is mapped to the material then the index should be set as -1 to indicate.

The next 8 bytes are 4 short values for blending. The first propery use_blending indicates if alpha-blending should be used or not. So 0 for "don't use blending", and 1 for "use blending". In the case that 1 is set, then the following three properties for the equation, src alpha, and dest alpha will be evaluated, otherwise these values will be ignored. So if blending is not desired all of these values can be defined as 0. The blend constants are defined as follows.

```
// Blend Equations

#define DASH_MATERIAL_NORMAL        0
#define DASH_MATERIAL_ADDITIVE      1
#define DASH_MATERIAL_SUBTRACT      2
#define DASH_MATERIAL_MULTIPLY      3

// Blend Contansts

#define DASH_MATERIAL_ZERO                  0
#define DASH_MATERIAL_ONE                   1
#define DASH_MATERIAL_SRC_COLOR             2
#define DASH_MATERIAL_ONE_MINUS_SRC_COLOR   3
#define DASH_MATERIAL_SRC_ALPHA             4
#define DASH_MATERIAL_ONE_MINUS_SRC_ALPHA   5
#define DASH_MATERIAL_DST_ALPHA             6
#define DASH_MATERIAL_ONE_MINUS_DST_ALPHA   7
#define DASH_MATERIAL_DST_COLOR             8
#define DASH_MATERIAL_ONE_MINUS_DST_COLOR   9
#define DASH_MATERIAL_SRC_ALPHA_SATURATE    10
```

The next value is skinning. In general most meshes in the Dash Model Format are assumed to be be rigged meshes. But some tools have a flag for if a specific model should acted as a skinned model or not, so this attribute has been included. In general for the Dash Fomat, if bones are declared, the assets is assumed to be skinned mesh, otherwise if the bone list has a length of 0, it's assumed to be a non-rigged mesh. But a non-rigged mesh is ecentially a rigged mesh to a single bone at the origin. For the shader type, the possible shader types are "basic", "lambert" and "phong". For "basic", only the diffuse color is used, for "lambert" emissive and diffuse color are evaluated. And for the last possible value "phong", diffuse, emmissive and specular are all evaluated. And if you're wondering why the options are limited and why there isn't metalic, the idea of the format is to define a simple format that can be viably supported by a wide range of applications. So more options could be added depending on necessity or support availability for a wide range of applications. 

```
// Shader Types

#define DASH_MATERIAL_BASIC        0
#define DASH_MATERIAL_LAMBERT      1
#define DASH_MATERIAL_PHONG        2
```

The next two properties are face side and shadow side. For face side I think the perferable option is to define the faces to be on the desired side in the first place depending on the winding order. I think this is mostly a flag for double-sided, but as long as it's there, we might as well add in the option for backside. And these same values can be used for the shadow side as well. The property for ignore lights is a boolean value, so 1 for "please ignore lights", otherwise 0 for "please calculate" light source. 

```
// Render Side

#define DASH_MATERIAL_FRONTSIDE     0
#define DASH_MATERIAL_BACKSIDE      1
#define DASH_MATERIAL_DOUBLESIDE    2
```

The next propery is vertex color, which indicates the material uses vertex color. The Dash Model Format approach is that everything should have vertex colors, as vertex colors are always defined in every face. If vertex colors are not needed, then vertex colors should be defined as white. This option has been included as it's an option found in some 3d applications, but in general this value should always should be assumed to be 1 for the Dash Model format approach, but you have the option to turn it off. For use alpha, this is a pretty common option, set 1 to indicate the material uses the alpha channel and 0 to indicate alpha should not be used. And the last proprty before the colors is the skip_render boolean. I'm not actually sure when or where this attribute is implemented. It seems to mostly be used in a case where a character holds multiple weapons and the weapon is to be skipped drawing or not. But really that should be done as a separate mesh, but this has been included since it seems like a pretty common attribute in several applications, as well as being implemented in several formats. For the alpha test property, this is a float from 0.0f to 1.0f if alpha test is desired. If it's not needed, it should be set at 0.0f.

Lastly we come to the different color channels. Diffuse is defined as four floats for red, green, blue and alpha. For emmissive, four floats are defined for red, green blue and intensity. How intense is the intensity? I really have no idea, it seems to depend on the application. And last we have specular which has four floats for red, green, blue and the specular coefficient or how [shiny](https://www.youtube.com/watch?v=93lrosBEW-Q) something is. Though this value seems to depend on the application, so we'll probably have to write some kind of scalar depending on the application. I think 30.0f should be considdered "standard" or something like that.

**JSON**

```
{
  "id" : int,
  "name" : string,
  "texture" : int,
  "use_blending" : bool,
  "blend_equation" : enum("norm", "add", "sub", "mult"),
  "blend_src" : enum("zero", "one", "src", "one_minus_src", "src_alpha", "one_minus_src_alpha", "dst_alpha", "one_minus_dst_alpha", "dst", "one_minus_dst"),
  "blend_dst" : enum("zero", "one", "src", "one_minus_src", "src_alpha", "one_minus_src_alpha", "dst_alpha", "one_minus_dst_alpha", "dst", "one_minus_dst"),
  "skinning" : bool,
  "shader_type" : enum("basic", "lambert", "phong")
  "face_side" : enum("front", "back", "double"),
  "shadow_side" : enum("front", "back", "double"),
  "ignore_lights" : bool,
  "vertex_color" : bool,
  "use_alpha" : bool,
  "skip_render" : bool,
  "alpha_test" : float,
  "diffuse" : float[4],
  "emissive" : float[4],
  "specular" : float[4]
}
```

The JSON format closely follows the properties defined by the binary format, with the main expection that the enum values can and probably should be defined as a specific string value from the given possibility of string values.

# 2-4 Vertices

**Binary**

Now we get into the vertex list which starts to demonstrate how the Dash Model Format takes rigged assets and distils them down into something simple. Each vertex in the binary vertex list is given by the following struct.

```
typedef struct {
	float pos[3];
	int16_t skin_index[4];
	float skin_weight[4];
} DashVertex_t;
```

The position is three floats for the x, y, and z coordinates of each vertex. Each vertex has up to four slots to be influenced by bones in the skeleton. The combined total of the weights should be 1.0f. The skin_index property gived the id of tbe bone in the skeleton to be influenced by. The value is a signed short value, so that -1 can be used to store the bind pose. The following weight value has four float, one for each of the slot values. The most common occurence will likely be a single bone weight suck as the example of skin_index: [4, 0, 0, 0] skin_weight: [1.0f, 0.0f, 0.0f, 0.0f] where 100% of the vertex is weighted to the bone index 4. For multiple influences skin_index: [4, 5, 0, 0] skin_weight: [0.75f, 0.25f, 0.0f, 0.0f], you have 75% of the vertex weight bound to bone index 4 and 25% to bone index 5. If no bones are declared, these values can be ignored, and the values can be filled with 0.

**JSON**

```
{
  "pos" : float[3],
  "skin_index" : int[4],
  "skin_weight" : float[4]
}
```

The JSON format of the vertex closely follows the binary format. The key values for each struct are given as the strings "pos", "skin_index", "skin_weight". The values for "pos", and "skin_weight" are float values, and "skin_index" should be whole (non-decimal) number values.

# 2-5 Faces

**Binary**

The Dash Model format is designed as an interchange format. So one of the design goals was to define vertices as single point with a specific weight. This was a model could be converted or imported into an editor, and allow the points to be manipulated. This is in contrast to a delivery format where all of the vertices are in a buffer that is already in a format that can be drawn directly into the GPU. In this format the positions and other other attributes are copied as many times as required in the order to draw the mesh. So in order to maintain the format as an interchange format, the following balance was decided on that a given vertex is a unique position with a unique weight, and then normals, uv's and vertex colors would all be defined per face indice. This allows for a very simple but powerful approach to model definitions that provides a lot of flexibility while using fixed-length structs. The struct for the face is given below.

```
typedef struct {
	uint16_t mat_id;
	uint16_t nop;
	uint32_t a;
	uint32_t b;
	uint32_t c;
	uint8_t aClr[4];
	uint8_t bClr[4];
	uint8_t cClr[4];
	float aNrm[3];
	float bNrm[3];
	float cNrm[3];
	float aUv[2];
	float bUv[2];
	float cUv[2];
} DashFace_t;
```

In the struct the first property is the material id. Textures are defined, materials are linked to textures, and faces reference a material. The following two bytes are not used in the interest of keeping to 4 byte boundaries. The next three properties are the a, b, c. These are indices that refer to indexes in the vertex list. They are 32 byte byte values as it was determined that 16 byte values didn't provide enough indices. So while the other id's for textures, materials and bones are all signed 16 byte values (to allow for -1), in this case there is no case where a vertex is not referenced for a triangle, and thus is unsigned. Each of the indices is specifically labeled 'a', 'b', and 'c' instead of an int array. This is is specically to demonstrate the relationship bewteen each index and the vertex color, normal and diffuse uv coordinate associated with each index.

For vertex color, colors are defined as four 8 bytes for RGBA for each indice respectively. This was used instead of four float values as it was deemed that using floats would cause too much unnecessary padding to the face list. Following that are normals for each indice. These are a vector float value with a combined sum of 1.0f to show the direction of the face for each index in the face. And finally is the diffuse uv coordinate for each of the face indices. If no diffuse texture is mapped to the material being used for this face, then these values can be left as 0.0f. 

**JSON**

```
{
  "material" : int,
  "indices" : [a, b, c],
  "verex_color" : [
    "rgba('255, 255, 255, 1.0)", 
    "rgba('255, 255, 255, 1.0)" , 
    "rgba('255, 255, 255, 1.0)"
  ],
  "normals" : [ float[3], float[3], float[3] ],
  "diffuse_uv": [ float[2], float[2], float[2] ]
}
```

The content of the JSON format of this struct is identical to the binary, but the provided strings are different from the binary struct. The material id is given by an id. The indices are provided in an array. And then each property for each indice are provided in their own arrays. Vertex color is provided as a CSS style, "rgba(255, 255, 255, 1.0)" string as alpha is expected in the binary format. The normals are an array of three float arrays for the x, y and z normal vector for each one of the faces. And the diffuse_uv values is a list of three float arrays with two float values for S and T respectively.

## 2-6 Skeleton

**Binary**

The primary intended use for the Dash Model Format is rigged models. So the skeleton for the model is assumed to be rigged to the mesh defined by the vertices and faces in the prior sections. The skeleton is made up of a list of bone structs with the following defintion.

```
typedef struct {
	char name[0x20];
	int16_t bone_id;
	int16_t parent_id;
	uint8_t nop[0x0c];
	float matrix[0x10];
} DashBone_t;
```

The first property in the struct is the bone name. Ideally this should be something like, "arm" or "leg" when possible, otherwise a name such as "bone_001", should be used. Specifically these names should not be empty and these names should be unique for each bone. As often the bone name will be used when creating animation curves for a given bone. 
The following property is the bone id, and similar to other id's this value is dictated by the bone's position in the array. So this number should be sequential starting with 0 for the root bone, and incrementing from there. 

The parent_id is the index for the parent bone. This value should be -1 for the root bone and be an index in the array after the root bone. The order of the bones should be structured so that parent bones are always defined before child bones. This seems obvious, but it is technically possible to create an out of order bone list that depends on indices. So when writing to a .dmf file it is always important to make sure this condition is set, so that bones can be parsed on a single loop for the respective import scripts.

The is 0x0c of padding for alignment to 0x10 boundaries, followed by the local transformation matrix. Admittedly this definition is a toss up between the local transformation matrix, and the individual transformation values for position, rotations and scale as vector3 values for each. In terms of approach for 3D applications, the local transformation matrix seems to be the most common, and if really required the original transformation values can be decomposed from the local transform matrix. It was also considdered to have both options included in the same bone, but that could present and issue where one value or the other wasn't calculated correctly and could result in compatibility issues between different importers and exporters. So it was chosen that only the transformation matrix be used. Also note that the alignment of the matrix is column-centric fit with the definition of most 3d applications based off OpenGL.

**JSON**

```
{
  name : string,
  id : int,
  parent_id : int,
  matrix : float[16]
}
```

In the JSON representation, the struct is nearly identical to the binary struct, without the need for binary padding.

# 2-7 Animations

**Binary**

Animations are defined by two structs. One struct which defines the animation, it's name and how long it is. And another struct that defines a specific key frame for a specific bone, which this document will refer to as a "track". The reason for this is to define the animation with only fixed length structs. So the first struct will provide definitions for all of the animations with a fixed length of structs, and then each animation is defined by an array of fixed length tracks. It makes the process very simple to break down and parse. The struct for the animation definion is given as follows.

```
typedef struct {
	char name[0x20];
	int16_t anim_id;
	uint16_t fps;
	uint32_t track_offset;
	uint32_t track_count;
	float duration;
} DashAnimation_t;
```

The first 32 bytes of the struct are for the animation name. Ideally this should be "run" or "walk", but similar to the bones, this value should be defined and should be unique. The anim_id property is pretty much entirely for show as there is nothing dependend on this index. It is included to make the file format as friendly as possible for reverse engineering to make the function of something easy for someone to understand when looking at the hexidecimal represention of the file format.

The next property is the "frames per second" or "fps", in most situations this value will likely be 30 or 60. The track_offset is the offset in the file to the list of animation tracks that define animation, and the track_count property is the length of the track list. The last property in the struct is a float value for the duration of the animation in seconds.

```
typedef struct {
	int16_t bone_id;
	int16_t prs_flags;
	float time;
	float pos[3];
	float rot[4];
	float scl[3];
} DashAnimTrack_t;
```

The animation track is a small innovation for the Dash Model Format. With most of the binary file formats I have experience with, the animation formats will often have a pointer to a list of bones, for which each bone value, there is a pointer if there are position values, rotation values, or scale values and pointers to where those lists can be found, and how many values there are. 

For the Dash Model Format, we use a direct list of fixed length structs to accomplish this. And the approach that we use is the first property in the struct is the bone_id to define the key frame for. The next value is a bitflag value for the "position", "rotation", and "scale" flags. In all cases places in the struct are reserved for each of these transformation, but whether or not they are evaluted depends on this bitflag. If the first bit is set, then position will be evaluated, if the second bit is set rotation will be evaluated, and if the third bit is set then scale will be evaluated. Note that the bit flag must have a bit set, so possible values are a single transformation, any combination of two transformation types or all transformation types. 

Following the bitflag is a float for the time value for the time of the keyframe in the animation. Following the time are the three reserved floats for the position, which are in x, y, z order. The rotation values are stored as quaternions in x, y, z, w order. And scale values are stored in x, y, z order.

**JSON**

```
{
  id : int,
  name : string, 
  fps : int,
  duration : float, 
  tracks : [
    {
      bone_id : int,
      time : float,
      pos : float[3], (if exists)
      rot : float[4], (if exists)
      scl : float[3] )if exists)
    },
    ...
  ]
}
```

The JSON representation of this struct largely reflects the contents of the binary struct. Like with other structs, rather than providing and offset and length for the tracks, the tracks are included as an array of objects directly. Otherwise the id, name, fps, and duration are as defined in the bianry section. For the animation track, the same rules hold true for the bone id and time. The difference for the transformations is that the transformations are only defined if they exist. And similar to the binary format, at least one transformation type should be defined, but the combination of any two, or all three can also be defined.


## Sample Model


![Dash Sample Model](https://gitlab.com/dashgl/format/-/raw/master/mascot/turnaround.gif)  

- Sample model provided by BeastPunks
- Model available under XX license (which means you can share and use as desired) (credit?)  
- BeastPunks website can be found here, and contact information here


## Official Mascot

**Dashie the Cyber Bunny** is a playful little bunny girl, young and eager to learn. Being a bunny reflects the library's small and nimble nature. Her low-polygon costume reflects the library's purpose of managing matrices and vectors. 

License: MIT Copyright (c) 2019 DashGL Project

Mascot character designed by **Tyson Tan**. Tyson Tan offers mascot design service for free and open source software, free of charge, under free license. Contact: [http://tysontan.com](http://tysontan.com) / [tysontan@mail.com](tysontan@mail.com)
