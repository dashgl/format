### Blender

The Blender folder contains the addon for Blender. Version 2.8.0 or greater
is required. The folder contains the file named **io_mesh_dmf.py**. This file
needs to be copied into the scripts/addon folder and then enabled from the
preference menu.
