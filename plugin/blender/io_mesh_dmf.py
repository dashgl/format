"""

 MIT License

 Copyright (c) 2019 Benjamin Collins (kion @ dashgl.com)

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

"""

DASH_MAGIC_NUMBER = 0x48534144;
DASH_MAJOR_VERSION = 0x01
DASH_MINOR_VERSION = 0x05

DASH_ANIMATION_POS = 0x01
DASH_ANIMATION_ROT = 0x02
DASH_ANIMATION_SCL = 0x04

# Required by Blender
import bpy
import math
from bpy.types import Operator
from bpy_extras.io_utils import ImportHelper, ExportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty

# Imported for Addon
import os
import tempfile
from struct import *
from mathutils import *
from bpy_extras.image_utils import load_image

bl_info = {
	'name' : "Dash Model Format version 1.5",
	'author' : "Benjamin Collins (Kion)",
	'version' : (0, 0, 3),
	'blender' : (2, 80, 0),
	'location' : 'File > Import-Export',
	'description' : 'Import-Export Dash Model Format',
	'warning' : '',
	'wiki_url' : 'https://gitlab.com/dashgl/format',
	'tracker_url' : 'https://gitlab.com/dashgl/format/issues',
	'category' : 'Import-Export',
	'support' : 'TESTING'
}

class DashMatrix:

	yup =  [
		(1.0, 0.0, 0.0, 0.0),
		(0.0, 0.0, 1.0, 0.0),
		(0.0, 1.0, 0.0, 0.0),
		(0.0, 0.0, 0.0, 1.0)
	]

	def __init__ (self, id, name):
		self.id = id
		self.name = name
		return None

	def setMatrix(self, a, b, c, d) :
		self.localMatrix = Matrix( (a, b, c, d) )
		self.worldMatrix = self.localMatrix
		return None
	
	def setParent(self, parent):
		b = parent.worldMatrix
		self.worldMatrix = DashMatrix.multiply(self.localMatrix, b)
		return None

	def getMatrix(self):
		mat = self.worldMatrix.transposed()
		return mat
		
	@staticmethod
	def multiply(a, b):
		tmp = Matrix()
		for i in range(4):
			for j in range(4):
				t = 0.0
				for k in range(4):
					t = t + (a[i][k] * b[k][j])
				tmp[i][j] = t
		return tmp

def register():
	classes = ( HandleImport, HandleExport )
	for c in classes:
		bpy.utils.register_class(c)

	bpy.types.TOPBAR_MT_file_import.append(HandleImport.menu_func)
	#bpy.types.TOPBAR_MT_file_export.append(HandleExport.menu_func)
	return None

def unregister():
	classes = ( HandleImport, HandleExport )
	for c in reversed(classes):
		bpy.utils.unregister_class(c)

	bpy.types.TOPBAR_MT_file_import.remove(HandleImport.menu_func)
	#bpy.types.TOPBAR_MT_file_export.remove(HandleExport.menu_func)
	return None

class HandleImport(Operator, ImportHelper):

	bl_idname = "import.dmf"
	bl_label = "Import Dash Model Format"

	filename_ext = ".dmf"
	filter_glob : StringProperty(default="*.dmf", options={'HIDDEN'})

	@staticmethod
	def menu_func(self, context):
		self.layout.operator(HandleImport.bl_idname, text="Dash Model Format (.dmf)")

	def execute(self, context):
		loader = DashLoader()
		res = loader.parse(self.filepath)

		if res == 0:
			return {'CANCELED'}

		return {'FINISHED'}

class DashLoader:

	def __init__ (self):

		# Create Internal data Structures

		self.header = {}
		self.textures = []
		self.vertices = []
		self.skin_index = []
		self.skin_weight = []
		self.faces = []
		self.colors = []
		self.normals = []
		self.uvs = []
		self.mat_index = []
		self.groups = []
		self.bones = []
		return None

	def parse(self, filepath):

		self.view = open(filepath, 'rb')
		
		# Check Magic Number and version
		
		data = self.view.read(0x10)
		s = unpack('IHHII', data)
		
		magic = s[0]
		major = s[1]
		minor = s[2]
		
		if magic != DASH_MAGIC_NUMBER:
			report('ERROR', 'Invalid Dash Magic number')
			return 0
		
		if major != DASH_MAJOR_VERSION:
			report('ERROR', 'Invalid Dash Major version')
			return 0
		
		if minor != DASH_MINOR_VERSION:
			report('ERROR', 'Invalid Dash Major version')
			return 0
		
		# Read File header for attribute count and offsets
				
		self.view.seek(0x18)
		data = self.view.read(0x8)
		s = unpack('II', data)
		self.header['texOffset'] = s[0]
		self.header['texCount'] = s[1]
		
		self.view.seek(0x28)
		data = self.view.read(0x8)
		s = unpack('II', data)
		self.header['matOffset'] = s[0]
		self.header['matCount'] = s[1]
		
		self.view.seek(0x38)
		data = self.view.read(0x8)
		s = unpack('II', data)
		self.header['posOffset'] = s[0]
		self.header['posCount'] = s[1]
		
		self.view.seek(0x48)
		data = self.view.read(0x8)
		s = unpack('II', data)
		self.header['faceOffset'] = s[0]
		self.header['faceCount'] = s[1]
		
		self.view.seek(0x58)
		data = self.view.read(0x8)
		s = unpack('II', data)
		self.header['boneOffset'] = s[0]
		self.header['boneCount'] = s[1]
		
		self.view.seek(0x68)
		data = self.view.read(0x8)
		s = unpack('II', data)
		self.header['animOffset'] = s[0]
		self.header['animCount'] = s[1]

		# Read Properties (Materials)

		self.mesh = bpy.data.meshes.new('Mesh')
		#self.mesh.name = os.path.basename(filepath).split(".")[0]
		self.mesh.name = "Dash Mesh"
		
		self.readTextures()
		self.readMaterials()
		
		# Read Properties (Geometry)
	
		self.readVertices()
		self.readFaces()

		# Create Blender Mesh and link

		self.mesh.from_pydata(self.vertices, [], self.faces)
		self.mesh.uv_layers.new()
		self.mesh.vertex_colors.new()

		uv_layer = self.mesh.uv_layers.active.data
		color_layer = self.mesh.vertex_colors.active.data

		for loop in self.mesh.loops:
			loop.normal = self.normals[loop.index]
			uv_layer[loop.index].uv = self.uvs[loop.index]
			color_layer[loop.index].color = self.colors[loop.index]

		for i in range(len(self.mat_index)):
			self.mesh.polygons[i].material_index = self.mat_index[i]
		
		self.mesh_obj = bpy.data.objects.new("DashMeshObj", self.mesh)
		bpy.context.layer_collection.collection.objects.link(self.mesh_obj)
		
		# Read Properties (skeleton)
		
		self.readBones()
		self.readAnimations()

		if len(self.bones) > 0:
			bpy.ops.object.mode_set(mode='OBJECT')
			self.mesh_obj.parent = self.arm_obj
			modifier = self.mesh_obj.modifiers.new(type='ARMATURE', name="Armature")
			modifier.object = self.arm_obj

			for group in self.groups:
				boneId = group['bone']
				indices = group['indices']
				weight = group['weight']
				self.mesh_obj.vertex_groups[boneId].add(indices, weight, "ADD")

			bpy.context.view_layer.objects.active = self.arm_obj
			bpy.ops.object.mode_set(mode="EDIT")
			self.arm_obj.rotation_euler = Euler((math.radians(90), 0, math.radians(90)), 'XYZ')
		else:
			bpy.context.view_layer.objects.active = self.mesh_obj
			bpy.ops.object.mode_set(mode="EDIT")
			self.mesh_obj.rotation_euler = Euler((math.radians(90), 0, math.radians(90)), 'XYZ')

		return 1
		

	def readTextures(self):
	
		print("Read Dash Model Textures")
		if self.header['texOffset'] == 0:
			return None
			
		self.view.seek(self.header['texOffset'])
		for i in range(0, self.header['texCount']):
		
			ofs = self.header['texOffset'] + (0x40 * i + 0)
			self.view.seek(ofs)
			data = self.view.read(0x20)
			s = unpack('32s', data)
			name = s[0].decode('ascii')
			
			data = self.view.read(0x14)
			s = unpack('hhIIHHHH', data)
			texId = s[0]
			flipY = s[1]
			offset = s[2]
			length = s[3]
			width = s[4]
			height = s[5]
			wrapS = s[6]
			wrapT = s[7]
			
			texture = bpy.data.textures.new(name, type='IMAGE')
			self.view.seek(offset)
			data = self.view.read(length)
			with tempfile.TemporaryDirectory() as tmpdir:
				img_path = os.path.join(tmpdir, 'texture_%03d' % texId)
				with open(img_path, 'wb') as f:
					f.write(data)
				png = load_image(img_path)
				png.pack()
				png.file_format = "PNG"
				texture.image = png
			self.textures.append(texture)
		
		return None
		
	def readMaterials(self):
		print("Read Dash Model Materials")
		if self.header['matOffset'] == 0:
			return None
			
		self.view.seek(self.header['matOffset'])
		for i in range(0, self.header['matCount']):
			data = self.view.read(0x20)
			s = unpack('32s', data)
			name = s[0].decode('ascii')
			
			data = self.view.read(0x50)
			s = unpack('hhHHHHHHHHHHHHfffffffffffff', data)
			matId = s[0]
			texId = s[1]
			mat = bpy.data.materials.new(name)
			mat.use_nodes = True
				
			if texId != -1:
				texture = self.textures[texId]
				bsdf = mat.node_tree.nodes["Principled BSDF"]
				texImage = mat.node_tree.nodes.new('ShaderNodeTexImage')
				texImage.image = texture.image
				mat.node_tree.links.new(bsdf.inputs['Base Color'], texImage.outputs['Color'])
			
			self.mesh.materials.append(mat)

		return None
	
	def readVertices(self):
	
		print("Read Dash Model Vertices")
		if self.header['posOffset'] == 0:
			return None
			
		self.view.seek(self.header['posOffset'])
		for i in range(self.header['posCount']):
			data = self.view.read(0x24)
			s = unpack('fffhhhhffff', data)
			x = s[0]
			y = s[1]
			z = s[2]
			self.vertices.append( [ x, y, z ] )
			indices = ( s[3], s[4], s[5], s[6] )
			weights = ( s[7], s[8], s[9], s[10] )

			for k in range(len(weights)) :
				if weights[k] == 0:
					continue
				self.groups.append({
					'bone' : indices[k],
					'indices' : [i],
					'weight' : weights[k]
				})
			
		return None
		
	def readFaces(self):
	
		print("Read Dash Model Faces")
		if self.header['faceOffset'] == 0:
			return None
		
		self.view.seek(self.header['faceOffset'])
		for i in range(0, self.header['faceCount']):
			data = self.view.read(0x58)
			s = unpack('hHIIIBBBBBBBBBBBBfffffffffffffff', data)

			self.mat_index.append(s[0])
			self.faces.append( (s[2], s[3], s[4]) )
			
			self.colors.append( ( s[5]/255, s[6]/255, s[7]/255, s[8]/255) )
			self.colors.append( ( s[9]/255, s[10]/255, s[11]/255, s[12]/255 ) )
			self.colors.append( ( s[13]/255, s[14]/255, s[15]/255, s[16]/255) )
			
			self.normals.append( Vector( (s[17], s[18], s[19]) ) )
			self.normals.append( Vector( (s[20], s[21], s[22]) ) )
			self.normals.append( Vector( (s[23], s[24], s[25]) ) )
			
			self.uvs.append( ( s[26], s[27] ) )
			self.uvs.append( ( s[28], s[29] ) )
			self.uvs.append( ( s[30], s[31] ) )

		return None

	def readBones(self):
	
		print("Read Dash Model Bones")
		if self.header['boneOffset'] == 0:
			return None
		
		armature = bpy.data.armatures.new('Dash Armature')
		self.arm_obj = bpy.data.objects.new('Dash Armature Object', armature)
		bpy.context.layer_collection.collection.objects.link(self.arm_obj)
		bpy.context.view_layer.objects.active = self.arm_obj
		bpy.ops.object.mode_set(mode="EDIT")
		self.armature = armature
		
		self.view.seek(self.header['boneOffset'])
		for i in range(self.header['boneCount']):
			
			data = self.view.read(0x20)
			s = unpack('32s', data)
			name = s[0].decode('ascii').rstrip(' \t\r\n\0')
			bone = DashMatrix(i, name)

			data = self.view.read(0x04)
			s = unpack('hh', data)
			boneId = s[0]
			parentId = s[1]
			
			self.view.read(0x0c)
			data = self.view.read(0x10)
			a = unpack('ffff', data)
			
			data = self.view.read(0x10)
			b = unpack('ffff', data)
			
			data = self.view.read(0x10)
			c = unpack('ffff', data)
			
			data = self.view.read(0x10)
			d = unpack('ffff', data)
			bone.setMatrix(a, b, c, d)
			
			arm_bone = armature.edit_bones.new(name)
			arm_bone.tail = Vector((0.0, 0.0, -1.0))

			if parentId != -1:
				arm_bone.parent = armature.edit_bones[parentId]
				bone.setParent(self.bones[parentId])

			self.bones.append(bone)
			self.mesh_obj.vertex_groups.new(name=bone.name)
			mat = bone.getMatrix()
			arm_bone.matrix = mat

			#if arm_bone.parent is not None and arm_bone.tail != arm_bone.parent.tail:
			#	arm_bone.head = arm_bone.parent.tail

		return None
	
	def readAnimations(self):
	
		print("Read Dash Model Animations")
		
		if self.header['animOffset'] == 0:
			return None

		if len(self.bones) == 0:
			return None
		
		animHeader = []
		
		self.view.seek(self.header['animOffset'])
		for i in range(0, self.header['animCount']):
			
			data = self.view.read(0x20)
			s = unpack('32s', data)
			name = s[0].decode('ascii')
			
			data = self.view.read(0x10)
			s = unpack('hHIIf', data)
			anim = {
				'name' : name,
				'id'  : s[0],
				'fps' : s[1],
				'offset' : s[2],
				'count' : s[3],
				'duration' : s[4],
				'tracks' : {}
			}
			
			for bone in self.bones:

				#anim['tracks']['pose.bones["%s"].location_x' %  bone.name] = []
				#anim['tracks']['pose.bones["%s"].location_y' %  bone.name] = []
				#anim['tracks']['pose.bones["%s"].location_z' %  bone.name] = []

				anim['tracks']['pose.bones["%s"].rotation_quaternion_x' %  bone.name] = []
				anim['tracks']['pose.bones["%s"].rotation_quaternion_y' %  bone.name] = []
				anim['tracks']['pose.bones["%s"].rotation_quaternion_z' %  bone.name] = []
				anim['tracks']['pose.bones["%s"].rotation_quaternion_w' %  bone.name] = []

				#anim['tracks']['pose.bones["%s"].scale_x' %  bone.name] = []
				#anim['tracks']['pose.bones["%s"].scale_y' %  bone.name] = []
				#anim['tracks']['pose.bones["%s"].scale_z' %  bone.name] = []
			
			animHeader.append(anim)
			
		for anim in animHeader:
		
			self.view.seek(anim['offset'])
			for i in range(0, anim['count']):
			
				data = self.view.read(0x30)
				s = unpack('hHfffffffffff', data)
				
				boneId = s[0]
				flags = s[1]
				time = s[2]
						
				frame = int(time * anim['fps'] + 0.5) + 1
				bone = self.bones[boneId]
				
				#if flags & DASH_ANIMATION_POS:
				#	anim['tracks']['pose.bones["%s"].location_x' %  bone.name].append( ( frame, s[3] ))
				#	anim['tracks']['pose.bones["%s"].location_y' %  bone.name].append( ( frame, s[4] ))
				#	anim['tracks']['pose.bones["%s"].location_z' %  bone.name].append( ( frame, s[5] ))
					
				if flags & DASH_ANIMATION_ROT:
					anim['tracks']['pose.bones["%s"].rotation_quaternion_x' %  bone.name].append( ( frame, s[6] ))
					anim['tracks']['pose.bones["%s"].rotation_quaternion_y' %  bone.name].append( ( frame, s[7] ))
					anim['tracks']['pose.bones["%s"].rotation_quaternion_z' %  bone.name].append( ( frame, s[8] ))
					anim['tracks']['pose.bones["%s"].rotation_quaternion_w' %  bone.name].append( ( frame, s[9] ))

				#if flags & DASH_ANIMATION_SCL:
				#	anim['tracks']['pose.bones["%s"].scale_x' %  bone.name].append( ( frame, s[10] ))
				#	anim['tracks']['pose.bones["%s"].scale_y' %  bone.name].append( ( frame, s[11] ))
				#	anim['tracks']['pose.bones["%s"].scale_z' %  bone.name].append( ( frame, s[12] ))
		
		self.arm_obj.animation_data_create()
		for anim in animHeader:
		
			action = bpy.data.actions.new(name=anim['name'])
			if self.arm_obj.animation_data.action is None:
				self.arm_obj.animation_data.action = action
			
			for key in anim['tracks']:

				if "location_x" in key:
					index = 0
				elif "location_y" in key:
					index = 1
				elif "location_z" in key:
					index = 2	
				elif "quaternion_x" in key:
					index = 1
				elif "quaternion_y" in key:
					index = 2
				elif "quaternion_z" in key:
					index = 3
				elif "quaternion_w" in key:
					index = 0
				elif "scale_x" in key:
					index = 0
				elif "scale_y" in key:
					index = 1
				elif "scale_z" in key:
					index = 2
					
				curve = action.fcurves.new(data_path=key[:-2], index=index)
				keyframe_points = curve.keyframe_points
				keyframe_points.add(len(anim['tracks'][key]))
				for i in range(len(anim['tracks'][key])):
					keyframe_points[i].co = anim['tracks'][key][i]

		return None

	# End DashLoader

class HandleExport(Operator, ExportHelper):

	bl_idname = "export.dmf"
	bl_label = "Export Dash Model Format"

	filename_ext = ".dmf"
	filter_glob : StringProperty(default="*.dmf", options={'HIDDEN'})
	
	@staticmethod
	def menu_func(self, context):
		default_path = bpy.data.filepath.replace(".blend", ".dmf")
		opts = self.layout.operator(HandleExport.bl_idname, text="Dash Model Format (.dmf)")
		opts.filepath = default_path

	def execute(self, context):
		print("Goodbye, World!!!")
		return {'FINISHED'}
