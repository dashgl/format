### Noesis

The Noesis folder contains the plugin for Noesis. The plugin was written for Noesis
version 4.395, so for best compatibility use this version or greater. Noesis can be
downloaded from [here](http://www.richwhitehouse.com/index.php?content=inc_projects.php&showproject=91). 
The folder contains the file named **fmt_dashgl_dmf.py**. Download this file and copy it
into the 'plugin/python' folder to add the file format to Noesis.