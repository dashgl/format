"""

 MIT License

 Copyright (c) 2019 Benjamin Collins (kion @ dashgl.com)

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

"""

from inc_noesis import *

"""
1. Define Contants
"""

DASH_MAGIC_NUMBER        = 0x48534144;
DASH_MAJOR_VERSION             = 0x01;
DASH_MINOR_VERSION             = 0x05;

# Header Label

DASH_LABEL_TEX           = 0x00786574;
DASH_LABEL_MAT           = 0x0074616D;
DASH_LABEL_VERT          = 0x74726576;
DASH_LABEL_FACE          = 0x65636166;
DASH_LABEL_BONE          = 0x656E6F62;
DASH_LABEL_ANIM          = 0x6D696E61;

# Texture Wrapping

DASH_TEXTURE_WRAP                 = 0;
DASH_TEXTURE_CLAMP                = 1;
DASH_TEXTURE_MIRROR               = 2;

# Image Formats

DASH_TEXTURE_PNG                  = 0;
DASH_TEXTURE_JPG                  = 1;
DASH_TEXTURE_TGA                  = 2;

# Pixel Format

DASH_TEXTURE_RGBA                 = 0;
DASH_TEXTURE_RGB                  = 1;

# Render Side

DASH_MATERIAL_FRONTSIDE           = 0;
DASH_MATERIAL_BACKSIDE            = 1;
DASH_MATERIAL_DOUBLESIDE          = 2;

# Shader Types

DASH_MATERIAL_BASIC               = 0;
DASH_MATERIAL_LAMBERT             = 1;
DASH_MATERIAL_PHONG               = 2;

# Blend Equations

DASH_MATERIAL_NORMAL              = 0;
DASH_MATERIAL_ADDITIVE            = 1;
DASH_MATERIAL_SUBTRACT            = 2;
DASH_MATERIAL_MULTIPLY            = 3;

# Blend Contansts

DASH_MATERIAL_ZERO                = 0;
DASH_MATERIAL_ONE                 = 1;
DASH_MATERIAL_SRC_COLOR           = 2;
DASH_MATERIAL_ONE_MINUS_SRC_COLOR = 3;
DASH_MATERIAL_SRC_ALPHA           = 4;
DASH_MATERIAL_ONE_MINUS_SRC_ALPHA = 5;
DASH_MATERIAL_DST_ALPHA           = 6;
DASH_MATERIAL_ONE_MINUS_DST_ALPHA = 7;
DASH_MATERIAL_DST_COLOR           = 8;
DASH_MATERIAL_ONE_MINUS_DST_COLOR = 9;
DASH_MATERIAL_SRC_ALPHA_SATURATE  = 10;

# Animation Flags

DASH_ANIMATION_POS             = 0x01;
DASH_ANIMATION_ROT             = 0x02;
DASH_ANIMATION_SCL             = 0x04;

"""
2. Register Addon
"""

def registerNoesisTypes():
	handle = noesis.register("Dash Model Format", ".dmf")
	noesis.setHandlerTypeCheck(handle, noepyCheckType)
	noesis.setHandlerLoadModel(handle, noepyLoadModel)
	noesis.setHandlerWriteModel(handle, noepyWriteModel)
	noesis.setHandlerWriteAnim(handle, noepyWriteAnim)
	noesis.logPopup()
	return 1

#check if it's this type based on the data
def noepyCheckType(data):
	if len(data) < 8:
		return 0

	bs = NoeBitStream(data)

	if bs.readUInt() != DASH_MAGIC_NUMBER:
		return 0
	if bs.readUShort() != DASH_MAJOR_VERSION:
		return 0
	if bs.readUShort() != DASH_MINOR_VERSION:
		return 0

	return 1

def noepyLoadModel(data, mdlList):

	loader = DashLoader()
	mdl = loader.parse(data)
	mdlList.append(mdl)
	rapi.setPreviewOption("setAnimPlay", "0")
	return 1

def noepyWriteModel(mdl, bs):

	exporter = DashExporter()
	exporter.parse(mdl)
	exporter.writeModel(bs)

	return 1

def noepyWriteAnim(anims, bs):
	if rapi.isGeometryTarget() == 0:
		print("WARNING: Stand-alone animations cannot be written to the .dmf format.")
		return 0

	rapi.setDeferredAnims(anims)
	return 1

"""
3. Export Class
"""

class DashExporter:

	def __init__(self):

		# Set offset at end of header

		self.ofs = 0x70

		# Set Header

		self.header = {
			'texOffset' : 0,
			'texCount' : 0,
			'matOffset' : 0,
			'matCount' : 0,
			'vertOffset' : 0,
			'vertCount' : 0,
			'vertPadding' : 0,
			'faceOffset' : 0,
			'faceCount' : 0,
			'facePadding' : 0,
			'boneOffset' : 0,
			'boneCount' : 0,
			'animOffset' : 0,
			'animCount' : 0
		}

		self.textureLookup = {}
		self.vertexLookup = {}

		# Prepare Properties

		self.lookup = {}
		self.textures = []
		self.materials = []
		self.vertices = []
		self.faces = []
		self.bones = []
		self.animations = []

		return None

	def parse(self, mdl):

		# Read Properties (material)

		texList = rapi.loadMdlTextures(mdl)
		self.readTextures(texList)
		self.readMaterials(mdl.modelMats)

		# Read Properties (Geometry)

		self.readVertices(mdl.meshes)
		self.readFaces(mdl.meshes, mdl.modelMats)

		#Read Properties (skeleton)

		anims = rapi.getDeferredAnims()
		self.readBones(mdl.bones)
		self.readAnimations(anims)

		return None

	def readTextures(self, texList):

		#Currently can't write PNG's, return
		#return None

		if len(texList) == 0:
			return None

		self.header['texOffset'] = self.ofs
		self.header['texCount'] = len(texList)

		self.ofs += 0x40 * len(texList)

		for i in range(0, len(texList)):

			name = texList[i].name
			width = texList[i].width
			height = texList[i].height
			self.lookup[name] = i

			print(texList[i])
			print(name)

			noesis.saveImageRGBA("test.png", texList[i])
			in_file = open("test.png", "rb")
			data = in_file.read()
			in_file.close()
			length = len(data)

			padding = 0
			if length % 16:
				diff = length % 16
				padding = 16 - diff
			offset = self.ofs
			self.ofs += (length + padding)
			flipY = 0



			tex = {
				'id' : i,
				'name' : name,
				'flipY' : 0,
				'offset' : offset,
				'length' : length,
				'width' : width,
				'height' : height,
				'padding' : padding,
				'wrapS' : 0,
				'wrapT' : 0,
				'img_format' : 2
			}
			tex['data'] = data
			self.textures.append(tex)


		print(self.lookup)
		return None

	def readMaterials(self, mats):

		matList = mats.matList
		if len(matList) == 0:
			return None

		self.header['matOffset'] = self.ofs
		self.header['matCount'] = len(matList)

		self.ofs += 0x70 * len(matList)

		for i in range(0, len(matList)):

			mat = {
				'name' : matList[i].name,
				'id' : i,
				'texId' : -1,
				'use_blend' : 0,
				'blendEquation' : 0,
				'blendSrc' : 0,
				'blendDst' : 0,
				'skinning' : 1,
				'shader_type' : 0,
				'render_side' : DASH_MATERIAL_FRONTSIDE,
				'shadow_side' : DASH_MATERIAL_FRONTSIDE,
				'ignore_light' : matList[i].disableLighting,
				'vertexColors' : 2,
				'use_alpha' : 1,
				'skip_render' : 0,
				'alphaTest' : matList[i].alphaTest,
				'diffuse' : matList[i].diffuseColor,
				'emissive' : matList[i].ambientColor,
				'specular' : matList[i].specularColor
			}
			print(matList[i].texName)
			if matList[i].texName in self.lookup:
				mat['texId'] = self.lookup[matList[i].texName]

			self.materials.append(mat)

		return None

	def readVertices(self, meshList):

		if len(meshList) == 0:
			return None

		self.lookup = []

		for mesh in meshList:

			meshIndex = len(self.lookup)
			self.lookup.append([])

			for i in range(0, len(mesh.positions)) :
				pos = mesh.positions[i]
				indices = [ 0, 0, 0, 0]
				weight = [ 0.0, 0.0, 0.0, 0.0 ]

				if i < len(mesh.weights):
					numWeight = mesh.weights[i].numWeights()
					if numWeight > 4:
						numWeight = 4
					for k in range(0, numWeight):
						indices[k] = mesh.weights[i].indices[k]
						weight[k] = mesh.weights[i].weights[k]

				key = "%.3f.%.3f.%.3f.%d.%d.%d.%d.%.3f.%.3f.%.3f.%.3f" % (
					pos[0], pos[1], pos[2],
					indices[0], indices[1], indices[2], indices[3],
					weight[0], weight[1], weight[2], weight[3]
				)

				if key not in self.vertexLookup:
					self.vertexLookup[key] = len(self.vertices)
					self.vertices.append({
						'pos' : pos,
						'indices' : indices,
						'weight' : weight
					})

				k = self.vertexLookup[key]
				self.lookup[meshIndex].append(k)

		if len(self.vertices) == 0:
			return None

		self.header['vertOffset'] = self.ofs
		self.header['vertCount'] = len(self.vertices)

		self.ofs += 0x24 * len(self.vertices)

		if self.ofs % 16:
			diff = 16 - (self.ofs % 16)
			self.header['vertPadding'] = diff
			self.ofs += diff

		return None

	def readFaces(self, meshList, mats):

		matList = mats.matList
		if len(meshList) == 0:
			return None

		meshIndex = -1
		for mesh in meshList:

			meshIndex += 1
			matIndex = -1
			for i in range(0, len(matList)):
				if mesh.matName != matList[i].name:
					continue
				matIndex = i
				break

			print(matIndex)
			hasColor = len(mesh.colors)
			hasNormal = len(mesh.normals)
			hasUV = len(mesh.uvs)

			print(len(mesh.positions))
			for i in range(0, len(mesh.indices), 3) :

				a = mesh.indices[i + 0]
				b = mesh.indices[i + 1]
				c = mesh.indices[i + 2]

				face = {}
				face['matIndex'] = matIndex
				face['a'] = self.lookup[meshIndex][a]
				face['b'] = self.lookup[meshIndex][b]
				face['c'] = self.lookup[meshIndex][c]
				face['aClr'] = [ 255, 255, 255, 255 ]
				face['bClr'] = [ 255, 255, 255, 255 ]
				face['cClr'] = [ 255, 255, 255, 255 ]
				face['aNrm'] = [ 0.0, 0.0, 0.0 ]
				face['bNrm'] = [ 0.0, 0.0, 0.0 ]
				face['cNrm'] = [ 0.0, 0.0, 0.0 ]
				face['aUv'] = [ 0.0, 0.0 ]
				face['bUv'] = [ 0.0, 0.0 ]
				face['cUv'] = [ 0.0, 0.0 ]

				if hasColor:
					face['aClr'] = mesh.colors[a]
					face['bClr'] = mesh.colors[b]
					face['cClr'] = mesh.colors[c]

					for p in range(0, 4):
						face['aClr'][p] = int(face['aClr'][p]*255)
						face['bClr'][p] = int(face['bClr'][p]*255)
						face['cClr'][p] = int(face['cClr'][p]*255)

				if hasNormal:
					face['aNrm'] = mesh.normals[a]
					face['bNrm'] = mesh.normals[b]
					face['cNrm'] = mesh.normals[c]

				if hasUV:
					face['aUv'] = mesh.uvs[a]
					face['bUv'] = mesh.uvs[b]
					face['cUv'] = mesh.uvs[c]

				self.faces.append(face)

		if len(self.faces) == 0:
			return None

		self.header['faceOffset'] = self.ofs
		self.header['faceCount'] = len(self.faces)
		self.ofs += 0x58 * len(self.faces)

		if self.ofs % 16:
			diff = 16 - (self.ofs % 16)
			self.header['facePadding'] = diff
			self.ofs += diff

		return None

	def readBones(self, bones):

		if len(bones) == 0:
			return None

		self.header['boneOffset'] = self.ofs
		self.header['boneCount'] = len(bones)
		self.ofs += 0x70 * len(bones)

		#matrixList = []
		for bone in bones:

			mtx = bone.getMatrix()

			dbone = {
				'name' : bone.name,
				'id' : bone.index,
				'parentId' : bone.parentIndex
			}

			if bone.parentIndex == -1:
				dbone['matrix'] = mtx.toMat44()
			else:
				inv = bones[bone.parentIndex].getMatrix().inverse()
				dbone['matrix'] = (mtx * inv).toMat44()

			#matrixList.append(mtx.inverse())
			self.bones.append(dbone)

		return None

	def readAnimations(self, animList):

		#Comment out animations (for now)
		#return None

		if len(animList) == 0:
			return None

		self.header['animOffset'] = self.ofs
		self.header['animCount'] = len(animList)
		self.ofs += 0x30 * len(animList)

		for i in range(0, len(animList)):

			print(animList[i].name)

			anim = {
				'id' : i,
				'fps' : int(animList[i].frameRate),
				'duration' : (animList[i].numFrames - 1) / animList[i].frameRate,
				'name' : animList[i].name,
				'tracks' : [],
				'offset' : self.ofs,
				'count' : 0
			}

			for bone in animList[i].bones:

				index = bone.index
				inv = bone.getMatrix().inverse()

				for k in range(0, animList[i].numFrames):

					m = animList[i].frameMats[index]
					index += len(animList[i].bones)

					pos = [ m[3][0], m[3][1], m[3][2] ]
					scl = [ m[0][0], m[1][1], m[2][2] ]
					q = (m * inv).toQuat()
					q = m.inverse().toQuat()
					rot = [ q[0], q[1], q[2], q[3] ]

					track = {
						'boneId' : bone.index,
						'flags' : DASH_ANIMATION_POS | DASH_ANIMATION_ROT | DASH_ANIMATION_SCL,
						'time' :  k / animList[i].frameRate,
						'pos' : pos,
						'rot' : rot,
						'scl' : [1, 1, 1]
					}

					anim['tracks'].append(track)

			self.animations.append(anim)
			self.ofs += 0x30 * len(anim['tracks'])

		return None

	def writeModel(self, bs):

		# Write Magic Number

		bs.writeUInt(DASH_MAGIC_NUMBER)
		bs.writeUShort(DASH_MAJOR_VERSION)
		bs.writeUShort(DASH_MINOR_VERSION)
		bs.writeUInt(0x10)
		bs.writeUInt(0x06)

		# Write Header

		bs.writeUInt(DASH_LABEL_TEX)
		bs.writeUInt(0)
		bs.writeUInt(self.header['texOffset'])
		bs.writeUInt(self.header['texCount'])

		bs.writeUInt(DASH_LABEL_MAT)
		bs.writeUInt(0)
		bs.writeUInt(self.header['matOffset'])
		bs.writeUInt(self.header['matCount'])

		bs.writeUInt(DASH_LABEL_VERT)
		bs.writeUInt(0)
		bs.writeUInt(self.header['vertOffset'])
		bs.writeUInt(self.header['vertCount'])

		bs.writeUInt(DASH_LABEL_FACE)
		bs.writeUInt(0)
		bs.writeUInt(self.header['faceOffset'])
		bs.writeUInt(self.header['faceCount'])

		bs.writeUInt(DASH_LABEL_BONE)
		bs.writeUInt(0)
		bs.writeUInt(self.header['boneOffset'])
		bs.writeUInt(self.header['boneCount'])

		bs.writeUInt(DASH_LABEL_ANIM)
		bs.writeUInt(0)
		bs.writeUInt(self.header['animOffset'])
		bs.writeUInt(self.header['animCount'])

		# Write Textures

		for tex in self.textures:
			name = tex['name']
			strlen = len(name)
			if strlen > 0x1f:
				name = name[0, 0x1f]
				strlen = 0x1f

			diff = 0x1f - strlen
			bs.writeString(name)
			for i in range (0, diff):
				bs.writeByte(0)

			bs.writeShort(tex['id'])
			bs.writeUShort(tex['flipY'])
			bs.writeUInt(tex['offset'])
			bs.writeUInt(tex['length'])
			bs.writeUShort(tex['width'])
			bs.writeUShort(tex['height'])
			bs.writeUShort(tex['wrapS'])
			bs.writeUShort(tex['wrapT'])

			# Fill Out 0x0c bytes
			bs.writeUInt(0)
			bs.writeUInt(0)
			bs.writeUInt(0)

		for tex in self.textures:
			print("writing bytes")
			bs.writeBytes(tex['data'])
			for i in range(0, tex['padding']):
				bs.writeByte(0)

		# Write Materials

		for mat in self.materials:
			name = mat['name']
			strlen = len(name)
			if strlen > 0x1f:
				name = name[0, 0x1f]
				strlen = 0x1f

			diff = 0x1f - strlen
			bs.writeString(name)
			for i in range (0, diff):
				bs.writeByte(0)

			bs.writeShort(mat['id'])
			bs.writeShort(mat['texId'])
			bs.writeUShort(mat['use_blend'])
			bs.writeUShort(mat['blendEquation'])
			bs.writeUShort(mat['blendSrc'])
			bs.writeUShort(mat['blendDst'])

			bs.writeUShort(mat['skinning'])
			bs.writeUShort(mat['shader_type'])
			bs.writeUShort(mat['render_side'])
			bs.writeUShort(mat['shadow_side'])

			bs.writeUShort(mat['ignore_light'])
			bs.writeUShort(mat['vertexColors'])
			bs.writeUShort(mat['use_alpha'])
			bs.writeUShort(mat['skip_render'])

			bs.writeFloat(mat['alphaTest'])

			bs.writeFloat(mat['diffuse'][0])
			bs.writeFloat(mat['diffuse'][1])
			bs.writeFloat(mat['diffuse'][2])
			bs.writeFloat(mat['diffuse'][3])

			bs.writeFloat(mat['emissive'][0])
			bs.writeFloat(mat['emissive'][1])
			bs.writeFloat(mat['emissive'][2])
			bs.writeFloat(mat['emissive'][3])

			bs.writeFloat(mat['specular'][0])
			bs.writeFloat(mat['specular'][1])
			bs.writeFloat(mat['specular'][2])
			bs.writeFloat(mat['specular'][3])

		# Write Vertices

		for vert in self.vertices:

			bs.writeFloat(vert['pos'][0])
			bs.writeFloat(vert['pos'][1])
			bs.writeFloat(vert['pos'][2])

			bs.writeUShort(vert['indices'][0])
			bs.writeUShort(vert['indices'][1])
			bs.writeUShort(vert['indices'][2])
			bs.writeUShort(vert['indices'][3])

			bs.writeFloat(vert['weight'][0])
			bs.writeFloat(vert['weight'][1])
			bs.writeFloat(vert['weight'][2])
			bs.writeFloat(vert['weight'][3])

		for i in range (0, self.header['vertPadding']):
			bs.writeByte(0)

		# Write Faces

		for face in self.faces:

			bs.writeShort(face['matIndex'])
			bs.writeShort(0)

			bs.writeUInt(face['a'])
			bs.writeUInt(face['b'])
			bs.writeUInt(face['c'])

			bs.writeByte(face['aClr'][0])
			bs.writeByte(face['aClr'][1])
			bs.writeByte(face['aClr'][2])
			bs.writeByte(face['aClr'][3])

			bs.writeByte(face['bClr'][0])
			bs.writeByte(face['bClr'][1])
			bs.writeByte(face['bClr'][2])
			bs.writeByte(face['bClr'][3])

			bs.writeByte(face['cClr'][0])
			bs.writeByte(face['cClr'][1])
			bs.writeByte(face['cClr'][2])
			bs.writeByte(face['cClr'][3])

			bs.writeFloat(face['aNrm'][0])
			bs.writeFloat(face['aNrm'][1])
			bs.writeFloat(face['aNrm'][2])

			bs.writeFloat(face['bNrm'][0])
			bs.writeFloat(face['bNrm'][1])
			bs.writeFloat(face['bNrm'][2])

			bs.writeFloat(face['cNrm'][0])
			bs.writeFloat(face['cNrm'][1])
			bs.writeFloat(face['cNrm'][2])

			bs.writeFloat(face['aUv'][0])
			bs.writeFloat(face['aUv'][1])

			bs.writeFloat(face['bUv'][0])
			bs.writeFloat(face['bUv'][1])

			bs.writeFloat(face['cUv'][0])
			bs.writeFloat(face['cUv'][1])

		for i in range (0, self.header['facePadding']):
			bs.writeByte(0)

		for bone in self.bones:
			name = bone['name']
			strlen = len(name)
			if strlen > 0x1f:
				name = name[0, 0x1f]
				strlen = 0x1f

			diff = 0x1f - strlen
			bs.writeString(name)
			for i in range (0, diff):
				bs.writeByte(0)

			bs.writeShort(bone['id'])
			bs.writeShort(bone['parentId'])
			bs.writeUInt(0)
			bs.writeUInt(0)
			bs.writeUInt(0)
			bs.writeBytes(bone['matrix'].toBytes())


		print("Exporting anims")

		for anim in self.animations:
			name = anim['name']
			strlen = len(name)
			if strlen > 0x1f:
				name = name[0, 0x1f]
				strlen = 0x1f

			diff = 0x1f - strlen
			bs.writeString(name)
			for i in range (0, diff):
				bs.writeByte(0)

			print(name)

			bs.writeShort(anim['id'])
			bs.writeUShort(anim['fps'])
			bs.writeUInt(anim['offset'])
			bs.writeUInt(len(anim['tracks']))
			bs.writeFloat(anim['duration'])

		for anim in self.animations:

			for track in anim['tracks']:
				bs.writeShort(track['boneId'])
				bs.writeUShort(track['flags'])
				bs.writeFloat(track['time'])

				bs.writeFloat(track['pos'][0])
				bs.writeFloat(track['pos'][1])
				bs.writeFloat(track['pos'][2])

				bs.writeFloat(track['rot'][0])
				bs.writeFloat(track['rot'][1])
				bs.writeFloat(track['rot'][2])
				bs.writeFloat(track['rot'][3])

				bs.writeFloat(track['scl'][0])
				bs.writeFloat(track['scl'][1])
				bs.writeFloat(track['scl'][2])

		return None

"""
4. Import Class
"""

class DashLoader:

	def __init__(self):

		self.textures = []
		self.materials = []
		self.vertices = [];
		self.weights = []
		self.indices = []
		self.normals = []
		self.colors = []
		self.uv = []
		self.bones = []
		self.anims = []
		self.meshes = []

		return None

	def parse(self, data):

		# Check Magic number and version

		self.bs = NoeBitStream(data)

		if self.bs.readUInt() != DASH_MAGIC_NUMBER:
			return None
		if self.bs.readUShort() != DASH_MAJOR_VERSION:
			return None
		if self.bs.readUShort() != DASH_MINOR_VERSION:
			return None

		# Parse Header

		self.header = {}
		self.bs.seek(0x18, NOESEEK_ABS)
		self.header['texOffset'] = self.bs.readUInt()
		self.header['texCount'] = self.bs.readUInt()

		self.bs.seek(0x28, NOESEEK_ABS)
		self.header['matOffset'] = self.bs.readUInt()
		self.header['matCount'] = self.bs.readUInt()

		self.bs.seek(0x38, NOESEEK_ABS)
		self.header['posOffset'] = self.bs.readUInt()
		self.header['posCount'] = self.bs.readUInt()

		self.bs.seek(0x48, NOESEEK_ABS)
		self.header['faceOffset'] = self.bs.readUInt()
		self.header['faceCount'] = self.bs.readUInt()

		self.bs.seek(0x58, NOESEEK_ABS)
		self.header['boneOffset'] = self.bs.readUInt()
		self.header['boneCount'] = self.bs.readUInt()

		self.bs.seek(0x68, NOESEEK_ABS)
		self.header['animOffset'] = self.bs.readUInt()
		self.header['animCount'] = self.bs.readUInt()

		# Read Properties (Materials)

		self.readTextures()
		self.readMaterials()

		# Read Properties (geometry)

		self.readVertices()
		self.readFaces()

		# Read Properties (skeleton)

		self.readBones()
		self.readAnimations()

		# Create Model

		#noeMesh = NoeMesh(self.indices, self.vertices)
		#noeMesh.setColors(self.colors)
		#noeMesh.setNormals(self.normals)
		#noeMesh.setUVs(self.uv)

		# If no bones remove weights (to avoid warnings)
		if len(self.bones) == 0:
			for mesh in self.meshes:
				mesh.setWeights([])

		# Create Model Materials
		noeMats = NoeModelMaterials(self.textures, self.materials)
		noeModel = NoeModel(self.meshes, self.bones, self.anims, noeMats)
		return noeModel

	def readTextures(self):

		print("Read Dash Model Textures")
		if self.header['texOffset'] == 0:
			return None

		texHeader = []
		for i in range (0, self.header['texCount']):

			ofs = self.header['texOffset'] + (0x40 * i + 0)
			self.bs.seek(ofs, NOESEEK_ABS)
			name = self.bs.readString()

			ofs = self.header['texOffset'] + (0x40 * i + 0x20)
			self.bs.seek(ofs, NOESEEK_ABS)

			tex = {
				'name' : name,
				'id' : self.bs.readShort(),
				'flipY' : self.bs.readShort(),
				'offset' : self.bs.readUInt(),
				'length' : self.bs.readUInt(),
				'width' : self.bs.readUShort(),
				'height' : self.bs.readUShort(),
				'wrapS' : self.bs.readUShort(),
				'wrapT' : self.bs.readUShort()
			}
			texHeader.append(tex)

		for tex in texHeader:
			self.bs.seek(tex['offset'], NOESEEK_ABS)
			sourceData = self.bs.readBytes(tex['length'])
			noeTex = rapi.loadTexByHandler(sourceData, '.png')
			noeTex.name = tex['name']
			self.textures.append(noeTex)

		return None

	def readMaterials(self):

		print("Read Dash Model Materials")
		if self.header['matOffset'] == 0:
			return None

		matHeader = []
		for i in range (0, self.header['matCount']):

			ofs = self.header['matOffset'] + (0x70 * i + 0)
			self.bs.seek(ofs, NOESEEK_ABS)
			name = self.bs.readString()

			ofs = self.header['matOffset'] + (0x70 * i + 0x20)
			self.bs.seek(ofs, NOESEEK_ABS)

			# Read the material
			mat = {
				'name' : name,
				'id' : self.bs.readShort(),
				'texId' : self.bs.readShort(),
				'use_blend' : self.bs.readUShort(),
				'blendEquation' : self.bs.readUShort(),
				'blendSrc' : self.bs.readUShort(),
				'blendDst' : self.bs.readUShort(),
				'skinning' : self.bs.readUShort(),
				'shader_type' : self.bs.readUShort(),
				'render_side' : self.bs.readUShort(),
				'shadow_side' : self.bs.readUShort(),
				'ignore_light' : self.bs.readUShort(),
				'vertex_colors' : self.bs.readUShort(),
				'use_alpha' : self.bs.readUShort(),
				'skip_render' : self.bs.readUShort(),
				'alphaTest' : self.bs.readFloat(),
				'diffuse' : [
					self.bs.readFloat(),
					self.bs.readFloat(),
					self.bs.readFloat(),
					self.bs.readFloat()
				],
				'emissive' : [
					self.bs.readFloat(),
					self.bs.readFloat(),
					self.bs.readFloat(),
					self.bs.readFloat()
				],
				'specular' : [
					self.bs.readFloat(),
					self.bs.readFloat(),
					self.bs.readFloat(),
					self.bs.readFloat()
				]
			}
			matHeader.append(mat)

			# Create mesh for each material
			mesh = NoeMesh([], [])
			mesh.setName("mesh_%03d" % i)
			mesh.setMaterial(name)
			self.meshes.append(mesh)

		for mat in matHeader:
			name = mat['name']

			noeMat = NoeMaterial(name, '')
			if mat['texId'] != -1:
				texName = self.textures[mat['texId']].name
				print(texName)
				noeMat.setTexture(texName)

			r = mat['diffuse'][0];
			g = mat['diffuse'][1];
			b = mat['diffuse'][2];
			a = mat['diffuse'][3];
			diffuse = NoeVec4([r, g, b, a])
			noeMat.setDiffuseColor(diffuse)
			#noeMat.setAlphaTest(mat['alphaTest'])
			self.materials.append(noeMat)

		return None

	def readVertices(self):

		print("Read Dash Model Vertices")
		if self.header['posOffset'] == 0:
			return None

		self.bs.seek(self.header['posOffset'], NOESEEK_ABS)
		for i in range (0, self.header['posCount']):
			pos = [
				self.bs.readFloat(),
				self.bs.readFloat(),
				self.bs.readFloat()
			]

			indices = [
				self.bs.readUShort(),
				self.bs.readUShort(),
				self.bs.readUShort(),
				self.bs.readUShort()
			]

			weight = [
				self.bs.readFloat(),
				self.bs.readFloat(),
				self.bs.readFloat(),
				self.bs.readFloat()
			]

			noeVert = NoeVec3(pos)
			noeWeight = NoeVertWeight(indices, weight)
			self.vertices.append(noeVert)
			self.weights.append(noeWeight)

		return None

	def readFaces(self):

		print("Read Dash Model Faces")
		if self.header['faceOffset'] == 0:
			return None

		self.bs.seek(self.header['faceOffset'], NOESEEK_ABS)
		for i in range (0, self.header['faceCount']):

			matId = self.bs.readShort()
			mesh = self.meshes[matId]
			self.bs.readShort()

			a = self.bs.readUInt()
			b = self.bs.readUInt()
			c = self.bs.readUInt()

			mesh.indices.append(len(mesh.indices))
			mesh.indices.append(len(mesh.indices))
			mesh.indices.append(len(mesh.indices))

			mesh.positions.append(self.vertices[a])
			mesh.positions.append(self.vertices[b])
			mesh.positions.append(self.vertices[c])

			mesh.weights.append(self.weights[a])
			mesh.weights.append(self.weights[b])
			mesh.weights.append(self.weights[c])

			aClr = NoeVec4([
				self.bs.readByte() / 255,
				self.bs.readByte() / 255,
				self.bs.readByte() / 255,
				self.bs.readByte() / 255
			])
			aClr[3] = 1;

			bClr = NoeVec4([
				self.bs.readByte() / 255,
				self.bs.readByte() / 255,
				self.bs.readByte() / 255,
				self.bs.readByte() / 255
			])
			bClr[3] = 1;

			cClr = NoeVec4([
				self.bs.readByte() / 255,
				self.bs.readByte() / 255,
				self.bs.readByte() / 255,
				self.bs.readByte() / 255
			])
			cClr[3] = 1;

			mesh.colors.append(aClr)
			mesh.colors.append(bClr)
			mesh.colors.append(cClr)

			aNrm = NoeVec3.fromBytes(self.bs.readBytes(0x0c))
			bNrm = NoeVec3.fromBytes(self.bs.readBytes(0x0c))
			cNrm = NoeVec3.fromBytes(self.bs.readBytes(0x0c))

			mesh.normals.append(aNrm)
			mesh.normals.append(bNrm)
			mesh.normals.append(cNrm)

			aUv = NoeVec3([
				self.bs.readFloat(),
				1 - self.bs.readFloat(),
				0
			])

			bUv = NoeVec3([
				self.bs.readFloat(),
				1 - self.bs.readFloat(),
				0
			])

			cUv = NoeVec3([
				self.bs.readFloat(),
				1 - self.bs.readFloat(),
				0
			])

			mesh.uvs.append(aUv)
			mesh.uvs.append(bUv)
			mesh.uvs.append(cUv)

		return None

	def readBones(self):

		print("Read Dash Model Bones")
		if self.header['boneOffset'] == 0:
			return None

		for i in range (0, self.header['boneCount']):

			ofs = self.header['boneOffset'] + (0x70 * i + 0)
			self.bs.seek(ofs, NOESEEK_ABS)
			name = self.bs.readString()

			ofs = self.header['boneOffset'] + (0x70 * i + 0x20)
			self.bs.seek(ofs, NOESEEK_ABS)

			id = self.bs.readShort()
			parentId = self.bs.readShort()

			self.bs.readBytes(0x0c)
			mat = NoeMat44.fromBytes(self.bs.readBytes(0x40))
			matrix = mat.toMat43()

			if i == 0:
				bone = NoeBone(id, name, matrix)
			else:
				parentName = self.bones[parentId].name
				matrix *= self.bones[parentId]._matrix
				bone = NoeBone(id, name, matrix, parentName, parentId)

			self.bones.append(bone)

		return None

	def readAnimations(self):

		print("Read Dash Model Animations")
		if self.header['animOffset'] == 0:
			return None

		animHeader = []
		for i in range (0, self.header['animCount']):
			ofs = self.header['animOffset'] + (0x30 * i + 0)
			self.bs.seek(ofs, NOESEEK_ABS)
			name = self.bs.readString()

			ofs = self.header['animOffset'] + (0x30 * i + 0x20)
			self.bs.seek(ofs, NOESEEK_ABS)
			anim = {
				'name' : name,
				'id' : self.bs.readShort(),
				'fps' : self.bs.readShort(),
				'offset' : self.bs.readUInt(),
				'length' : self.bs.readUInt(),
				'duration' : self.bs.readFloat()
			}
			animHeader.append(anim)

		for anim in animHeader:

			self.bs.seek(anim['offset'], NOESEEK_ABS)
			animKeys = []

			for i in range (0, anim['length']):
				boneId = self.bs.readShort()

				flags = self.bs.readShort()

				if len(animKeys) == boneId:
					animKeys.append({
						'pos' : [],
						'scl' : [],
						'rot' : []
					})

				time = self.bs.readFloat()

				pos = NoeVec3([
					self.bs.readFloat(),
					self.bs.readFloat(),
					self.bs.readFloat()
				])

				if flags & DASH_ANIMATION_POS:
					posKey = NoeKeyFramedValue(time, pos)
					animKeys[boneId]['pos'].append(posKey)

				mq =[
					self.bs.readFloat(),
					self.bs.readFloat(),
					self.bs.readFloat(),
					self.bs.readFloat()
				]
				rot = NoeQuat([mq[0], mq[1], mq[2], -mq[3]])

				if flags & DASH_ANIMATION_ROT:
					rotKey = NoeKeyFramedValue(time, rot)
					animKeys[boneId]['rot'].append(rotKey)

				scl = NoeVec3([
					self.bs.readFloat(),
					self.bs.readFloat(),
					self.bs.readFloat()
				])

				if flags & DASH_ANIMATION_SCL:
					sclKey = NoeKeyFramedValue(time, scl)
					animKeys[boneId]['scl'].append(sclKey)

			keyBones = []
			for i in range(0, len(animKeys)):

				kfBone = NoeKeyFramedBone(i)
				kfBone.setTranslation(animKeys[i]['pos'])
				kfBone.setRotation(animKeys[i]['rot'])
				kfBone.setScale(animKeys[i]['scl'], noesis.NOEKF_SCALE_VECTOR_3)
				keyBones.append(kfBone)

			name = anim['name']
			noeAnim = NoeKeyFramedAnim(name, self.bones, keyBones, anim['fps'])
			self.anims.append(noeAnim)

		return None

"""
5. End
"""
