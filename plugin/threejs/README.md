### Threejs

The Threejs folder contains two files, **DashExporter.js** and **DashLoader.js**. To 
be able to load .dmf files and import them into a scene.

### Import

Include the DashLoader script file into your project.

```
<script type="text/javascript" src="DashLoader.js"></script>
```

Import a file using the standard callback

```
let dashLoader = new THREE.DashLoader();
dashLoader.load("dat/ServBot.dmf", function(err, mesh) {
    if(err) {
        throw err;
    }
    scene.add(mesh);
}
```

Import a file using the async callback

```
async function loadModel(url) {

    let dashLoader = new THREE.DashLoader();

    let mesh;
    try {
        mesh = await dashLoader.loadAsync(url);
    } catch(err) {
        throw err;
    }
    scene.add(mesh);
}
```

### Export

Include the DashExporter script file into your project.

```
<script type="text/javascript" src="DashExporter.js"></script>
```

The exporter generates a blob. To save the blob to your local machine it is recomended to use
something like the [FileSaver](https://github.com/eligrey/FileSaver.js/) to save the file to your
local hardrive.


```
let dashExporter = new THREE.DashExporter();
let blob = dashExporter.parse(mesh);
saveAs(blob, mesh.name + ".dmf");
```

T