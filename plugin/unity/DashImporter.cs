/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019 Andreas Scholl (andreas @ kritzelkratz.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/
 
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Threading;
using DashGL;

namespace DashImporter
{
    public class Importer
    {
        public enum BlendMode
        {
            Opaque,
            Cutout,
            Fade,       // Old school alpha-blending mode, fresnel does not affect amount of transparency
            Transparent // Physically plausible transparency mode, implemented as alpha pre-multiply
        }

        [System.Serializable]
        public class GameObjectEvent : UnityEvent<GameObject>
        {
        }

        public class ImportTask
        {
            public bool Finished;

            public GameObjectEvent FinishedEvent;
        }

        private ImportTask task;

        private static string _sourcePath;
//        public static NodeData[] _rootNodes;

        private static Thread _thread;

        private static bool _threadRunning;
        private static bool _workDone;

        public static bool LoadAndParseFinished = false;

        private DashLoader _loader;

        public List<Texture2D> _textures = new List<Texture2D>();

        public ImportTask Import(string sourcePath, UnityAction<GameObject> finishedAction)
        {
            _loader = new DashLoader();

            // create import task
            //
            task = new ImportTask();
            task.Finished = false;
            task.FinishedEvent = new GameObjectEvent();
            task.FinishedEvent.AddListener(finishedAction);

            Coroutiner.Instance.StartNonMonoCoroutine(ImportCoroutine(sourcePath));

            return task;
        }

        private IEnumerator ImportCoroutine(string sourcePath)
        {
            // get unity obj. name
            string objectName = FileSystemHelper.GetFileNameWithoutExtensionFromPath(sourcePath);

            // load and parse fbx via thread
            //
            LoadAndParseAsync(sourcePath);

            while (LoadAndParseFinished == false)
            {
                yield return null;
            }

            // create textures
            //
            List<Byte[]> textures = _loader.GetTextures();
            List<ImageAttributes> texAttributes = _loader.GetTextureAttributes();

            for (int count = 0; count < textures.Count; count++)
            {
                Texture2D texture = new Texture2D(16, 16);
                texture.LoadImage(textures[count]);

                _textures.Add(texture);

                yield return null;
            }

            // import / create meshes
            //
            MeshCreator meshCreator = new MeshCreator();
            Mesh mesh = meshCreator.CreateMesh(_loader);

            yield return null;

            GameObject go = new GameObject();

//            MeshFilter filter = go.AddComponent<MeshFilter>();
//            filter.mesh = mesh;

//            MeshRenderer renderer = go.AddComponent<MeshRenderer>();
            SkinnedMeshRenderer renderer = go.AddComponent<SkinnedMeshRenderer>();
            renderer.sharedMesh = mesh;

            // build bones hierarchy
            //
            List<BoneAttributes> bones = _loader.GetBoneAttributes();
            List<GameObject> boneObjects = new List<GameObject>();
            List<Transform> boneTransforms = new List<Transform>();
            List<Matrix4x4> bindPoses = new List<Matrix4x4>();

            // add all bones
            foreach (BoneAttributes bone in bones)
            {
                GameObject boneObject = new GameObject(bone.name);
                boneObjects.Add(boneObject);
            }

            // setup bone hierarchy
            int boneCount = 0;
            foreach (BoneAttributes bone in bones)
            {
                GameObject boneObject = boneObjects[boneCount];
                if (bone.parentId == -1)
                {
                    boneObject.transform.parent = go.transform;
                }
                else if(bone.parentId < boneObjects.Count)
                {
                    boneObject.transform.parent = boneObjects[bone.parentId].transform;
                }

                Matrix4x4 matrix = bone.matrix;

                boneObject.transform.localPosition = MatrixHelper.ExtractTranslationFromMatrix(ref matrix);
                boneObject.transform.localRotation = MatrixHelper.ExtractRotationFromMatrix(ref matrix);
                boneObject.transform.localScale = MatrixHelper.ExtractScaleFromMatrix(ref matrix);

                boneTransforms.Add(boneObject.transform);

                Matrix4x4 bindPose = new Matrix4x4();
                bindPose = boneObject.transform.worldToLocalMatrix * go.transform.localToWorldMatrix;
                bindPoses.Add(bindPose);

                boneCount++;
            }

            mesh.bindposes = bindPoses.ToArray();
            renderer.bones = boneTransforms.ToArray();

            // create materials
            //
            List<Material> materials = new List<Material>();
            List<MaterialAttributes> matAttributes = _loader.GetMaterialAttributes();

            for (int count = 0; count < matAttributes.Count; count++)
            {
                Material material = new Material(Shader.Find("Standard"));

                int textureId = matAttributes[count].textureId;
                material.mainTexture = _textures[textureId];

                Vector2 textureScale = Vector2.one;
                if (texAttributes[textureId].flipY == 0)
                {
                    textureScale.y = -1f;
                }
                material.SetTextureScale("_MainTex", textureScale);

                ChangeRenderMode(material, BlendMode.Cutout);

                materials.Add(material);
            }

            renderer.sharedMaterials = materials.ToArray();

            go.name = objectName;

            // create animations / clips
            //
            Animation anim = go.AddComponent<Animation>();

            List<AnimationAttributes> animationAttributes = _loader.GetAnimationAttributes();

            foreach (AnimationAttributes animationAttribute in animationAttributes)
            {
                AnimationClip clip = new AnimationClip();

                foreach (HierarchyAttributes hierarchy in animationAttribute.hierarchy)
                {
                    // prepare single value keyframe lists
                    //
                    List<Keyframe> positionXList = new List<Keyframe>();
                    List<Keyframe> positionYList = new List<Keyframe>();
                    List<Keyframe> positionZList = new List<Keyframe>();

                    List<Keyframe> rotationXList = new List<Keyframe>();
                    List<Keyframe> rotationYList = new List<Keyframe>();
                    List<Keyframe> rotationZList = new List<Keyframe>();
                    List<Keyframe> rotationWList = new List<Keyframe>();

                    List<Keyframe> scaleXList = new List<Keyframe>();
                    List<Keyframe> scaleYList = new List<Keyframe>();
                    List<Keyframe> scaleZList = new List<Keyframe>();

                    foreach (KeyAttributes key in hierarchy.keys)
                    {
                        Keyframe animKey;

                        if (key.usePos == true)
                        {
                            animKey = new Keyframe(key.time, key.pos.x);
                            positionXList.Add(animKey);
                            animKey = new Keyframe(key.time, key.pos.y);
                            positionYList.Add(animKey);
                            animKey = new Keyframe(key.time, key.pos.z);
                            positionZList.Add(animKey);
                        }

                        if (key.useRot == true)
                        {
                            animKey = new Keyframe(key.time, key.rot.x);
                            rotationXList.Add(animKey);
                            animKey = new Keyframe(key.time, key.rot.y);
                            rotationYList.Add(animKey);
                            animKey = new Keyframe(key.time, key.rot.z);
                            rotationZList.Add(animKey);
                            animKey = new Keyframe(key.time, key.rot.w);
                            rotationWList.Add(animKey);
                        }

                        if (key.useScl == true)
                        {
                            animKey = new Keyframe(key.time, key.scl.x);
                            scaleXList.Add(animKey);
                            animKey = new Keyframe(key.time, key.scl.y);
                            scaleYList.Add(animKey);
                            animKey = new Keyframe(key.time, key.scl.z);
                            scaleZList.Add(animKey);
                        }
                    }

                    // create animation curves
                    //
                    int parent = hierarchy.parent;
                    Transform childTransform = go.transform;
                    if (parent >= 0)
                    {
                        childTransform = boneObjects[parent].transform;
                    }
                    string childPath = GetChildPath(go.transform, childTransform);

                    AnimationCurve curve;

                    if (positionXList.Count > 0)
                    {
                        curve = new AnimationCurve();
                        curve.keys = positionXList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalPosition.x", curve);

                        curve = new AnimationCurve();
                        curve.keys = positionYList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalPosition.y", curve);

                        curve = new AnimationCurve();
                        curve.keys = positionZList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalPosition.z", curve);
                    }

                    if (rotationXList.Count > 0)
                    {
                        curve = new AnimationCurve();
                        curve.keys = rotationXList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalRotation.x", curve);

                        curve = new AnimationCurve();
                        curve.keys = rotationYList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalRotation.y", curve);

                        curve = new AnimationCurve();
                        curve.keys = rotationZList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalRotation.z", curve);

                        curve = new AnimationCurve();
                        curve.keys = rotationWList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalRotation.w", curve);
                    }

                    if (scaleXList.Count > 0)
                    {
                        curve = new AnimationCurve();
                        curve.keys = scaleXList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalScale.x", curve);

                        curve = new AnimationCurve();
                        curve.keys = scaleYList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalScale.y", curve);

                        curve = new AnimationCurve();
                        curve.keys = scaleZList.ToArray();
                        clip.SetCurve(childPath, typeof(Transform), "m_LocalScale.z", curve);
                    }
                }

                clip.EnsureQuaternionContinuity();
                clip.legacy = true;
                clip.wrapMode = WrapMode.Loop;  // TEST!
                anim.AddClip(clip, animationAttribute.name);
            }

            task.Finished = true;
            task.FinishedEvent.Invoke(go);

            yield return null;

        }

        public static string GetChildPath(Transform rootTransform, Transform childTransform)
        {
            string path = childTransform.name;
            while (childTransform.parent != rootTransform)
            {
                childTransform = childTransform.parent;
                path = childTransform.name + "/" + path;
            }
            return path;
        }

        private void ChangeRenderMode(Material standardShaderMaterial, BlendMode blendMode)
        {
            switch (blendMode)
            {
                case BlendMode.Opaque:
                    standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    standardShaderMaterial.SetInt("_ZWrite", 1);
                    standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                    standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                    standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    standardShaderMaterial.renderQueue = -1;
                    break;
                case BlendMode.Cutout:
                    standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    standardShaderMaterial.SetInt("_ZWrite", 1);
                    standardShaderMaterial.EnableKeyword("_ALPHATEST_ON");
                    standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                    standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    standardShaderMaterial.renderQueue = 2450;
                    break;
                case BlendMode.Fade:
                    standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                    standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    standardShaderMaterial.SetInt("_ZWrite", 0);
                    standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                    standardShaderMaterial.EnableKeyword("_ALPHABLEND_ON");
                    standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    standardShaderMaterial.renderQueue = 3000;
                    break;
                case BlendMode.Transparent:
                    standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    standardShaderMaterial.SetInt("_ZWrite", 0);
                    standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                    standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                    standardShaderMaterial.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                    standardShaderMaterial.renderQueue = 3000;
                    break;
            }

        }

        public void CancelImport()
        {
            if (_threadRunning == true)
            {
                _threadRunning = false;
                _thread.Join();
            }
        }

        public void LoadAndParseAsync(string sourcePath)
        {
            _sourcePath = sourcePath;
//            _rootNodes = null;

            LoadAndParseFinished = false;

            _thread = new Thread(LoadAndParse);
            _thread.Start();
        }

        public void LoadAndParse()
        {
            _threadRunning = true;
            _workDone = false;

            while (_threadRunning && !_workDone)
            {
                try
                {
                    bool successful = _loader.LoadAndParse(_sourcePath);

                    if (successful)
                    {
                        // perform clean-up => nothing to do for dash-loader
                    }
                }
                catch (Exception e)
                {
                    Debug.Log(e.Message);
                }

                _workDone = true;
            }

            _threadRunning = false;
            _workDone = true;

            LoadAndParseFinished = true;
        }
/*
        public void ProgressDelegate(float progress)
        {
        }

        public void ReaderInfo(string message)
        {
            Debug.Log(message);
        }

        public void ReaderWarning(string message)
        {
            Debug.Log(message);
        }

        public void ReaderError(string message)
        {
            Debug.Log(message);
        }
*/
    }
}