
namespace DashGL
{
    public class DashConstants
    {
        // Magic Number
        public const uint DASH_MAGIC_NUMBER = 0x48534144;
        public const uint DASH_MAJOR_VERSION = 0x01;
        public const uint DASH_MINOR_VERSION = 0x05;
        //        const uint DASH_MIME_TYPE = { type : 'application/octet-stream' };

        // Header Label
        public const uint DASH_LABEL_TEX = 0x00786574;
        public const uint DASH_LABEL_MAT = 0x0074616D;
        public const uint DASH_LABEL_VERT = 0x74726576;
        public const uint DASH_LABEL_FACE = 0x65636166;
        public const uint DASH_LABEL_BONE = 0x656E6F62;
        public const uint DASH_LABEL_ANIM = 0x6D696E61;

        // Texture Wrapping
        public const uint DASH_TEXTURE_WRAP = 0;
        public const uint DASH_TEXTURE_CLAMP = 1;
        public const uint DASH_TEXTURE_MIRROR = 2;

        // Image Formats
        public const uint DASH_TEXTURE_PNG = 0;
        public const uint DASH_TEXTURE_JPG = 1;
        public const uint DASH_TEXTURE_TGA = 2;

        // Pixel Format
        public const uint DASH_TEXTURE_RGBA = 0;
        public const uint DASH_TEXTURE_RGB = 1;

        // Render Side
        public const uint DASH_MATERIAL_FRONTSIDE = 0;
        public const uint DASH_MATERIAL_BACKSIDE = 1;
        public const uint DASH_MATERIAL_DOUBLESIDE = 2;

        // Shader Types
        public const uint DASH_MATERIAL_BASIC = 0;
        public const uint DASH_MATERIAL_LAMBERT = 1;
        public const uint DASH_MATERIAL_PHONG = 2;

        // Blend Equations
        public const uint DASH_MATERIAL_NORMAL = 0;
        public const uint DASH_MATERIAL_ADDITIVE = 1;
        public const uint DASH_MATERIAL_SUBTRACT = 2;
        public const uint DASH_MATERIAL_MULTIPLY = 3;

        // Blend Contansts
        public const uint DASH_MATERIAL_ZERO = 0;
        public const uint DASH_MATERIAL_ONE = 1;
        public const uint DASH_MATERIAL_SRC_COLOR = 2;
        public const uint DASH_MATERIAL_ONE_MINUS_SRC_COLOR = 3;
        public const uint DASH_MATERIAL_SRC_ALPHA = 4;
        public const uint DASH_MATERIAL_ONE_MINUS_SRC_ALPHA = 5;
        public const uint DASH_MATERIAL_DST_ALPHA = 6;
        public const uint DASH_MATERIAL_ONE_MINUS_DST_ALPHA = 7;
        public const uint DASH_MATERIAL_DST_COLOR = 8;
        public const uint DASH_MATERIAL_ONE_MINUS_DST_COLOR = 9;
        public const uint DASH_MATERIAL_SRC_ALPHA_SATURATE = 10;

        // Animation Flags
        public const uint DASH_ANIMATION_POS = 0x01;
        public const uint DASH_ANIMATION_ROT = 0x02;
        public const uint DASH_ANIMATION_SCL = 0x04;
    }
}
