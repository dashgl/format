/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019 Andreas Scholl (andreas @ kritzelkratz.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/
 
using UnityEngine;

public class MatrixHelper
{

	/// <summary>
	/// Extract translation from transform matrix.
	/// </summary>
	/// <param name="matrix">Transform matrix. This parameter is passed by reference
	/// to improve performance; no changes will be made to it.</param>
	/// <returns>
	/// Translation offset.
	/// </returns>
	public static Vector3 ExtractTranslationFromMatrix(ref Matrix4x4 matrix) {
		Vector3 translate;
		translate.x = matrix.m03;
		translate.y = matrix.m13;
		translate.z = matrix.m23;
		return translate;
	}

	/// <summary>
	/// Extract rotation quaternion from transform matrix.
	/// </summary>
	/// <param name="matrix">Transform matrix. This parameter is passed by reference
	/// to improve performance; no changes will be made to it.</param>
	/// <returns>
	/// Quaternion representation of rotation transform.
	/// </returns>
	public static Quaternion ExtractRotationFromMatrix(ref Matrix4x4 matrix) {
		Vector3 forward;
		forward.x = matrix.m02;
		forward.y = matrix.m12;
		forward.z = matrix.m22;

		Vector3 upwards;
		upwards.x = matrix.m01;
		upwards.y = matrix.m11;
		upwards.z = matrix.m21;

		return Quaternion.LookRotation(forward, upwards);
	}

	/// <summary>
	/// Extract scale from transform matrix.
	/// </summary>
	/// <param name="matrix">Transform matrix. This parameter is passed by reference
	/// to improve performance; no changes will be made to it.</param>
	/// <returns>
	/// Scale vector.
	/// </returns>
	public static Vector3 ExtractScaleFromMatrix(ref Matrix4x4 matrix) {
		Vector3 scale;
		scale.x = new Vector4(matrix.m00, matrix.m10, matrix.m20, matrix.m30).magnitude;
		scale.y = new Vector4(matrix.m01, matrix.m11, matrix.m21, matrix.m31).magnitude;
		scale.z = new Vector4(matrix.m02, matrix.m12, matrix.m22, matrix.m32).magnitude;

        if (Vector3.Cross(matrix.GetColumn(0), matrix.GetColumn(1)).normalized != (Vector3)matrix.GetColumn(2).normalized)
        {
            scale.x *= -1;
        }

        return scale;
	}

	/// <summary>
	/// Extract position, rotation and scale from TRS matrix.
	/// </summary>
	/// <param name="matrix">Transform matrix. This parameter is passed by reference
	/// to improve performance; no changes will be made to it.</param>
	/// <param name="localPosition">Output position.</param>
	/// <param name="localRotation">Output rotation.</param>
	/// <param name="localScale">Output scale.</param>
	public static void DecomposeMatrix(ref Matrix4x4 matrix, out Vector3 localPosition, out Quaternion localRotation, out Vector3 localScale) {
		localPosition = ExtractTranslationFromMatrix(ref matrix);
		localRotation = ExtractRotationFromMatrix(ref matrix);
		localScale = ExtractScaleFromMatrix(ref matrix);
	}

	/// <summary>
	/// Set transform component from TRS matrix.
	/// </summary>
	/// <param name="transform">Transform component.</param>
	/// <param name="matrix">Transform matrix. This parameter is passed by reference
	/// to improve performance; no changes will be made to it.</param>
	public static void SetTransformFromMatrix(Transform transform, ref Matrix4x4 matrix) {
		transform.localPosition = ExtractTranslationFromMatrix(ref matrix);
		transform.localRotation = ExtractRotationFromMatrix(ref matrix);
		transform.localScale = ExtractScaleFromMatrix(ref matrix);
	}


	// EXTRAS!

	/// <summary>
	/// Identity quaternion.
	/// </summary>
	/// <remarks>
	/// <para>It is faster to access this variation than <c>Quaternion.identity</c>.</para>
	/// </remarks>
	public static readonly Quaternion IdentityQuaternion = Quaternion.identity;
	/// <summary>
	/// Identity matrix.
	/// </summary>
	/// <remarks>
	/// <para>It is faster to access this variation than <c>Matrix4x4.identity</c>.</para>
	/// </remarks>
	public static readonly Matrix4x4 IdentityMatrix = Matrix4x4.identity;

	/// <summary>
	/// Get translation matrix.
	/// </summary>
	/// <param name="offset">Translation offset.</param>
	/// <returns>
	/// The translation transform matrix.
	/// </returns>
	public static Matrix4x4 TranslationMatrix(Vector3 offset) {
		Matrix4x4 matrix = IdentityMatrix;
		matrix.m03 = offset.x;
		matrix.m13 = offset.y;
		matrix.m23 = offset.z;
		return matrix;
	}
}
