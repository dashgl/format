/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019 Andreas Scholl (andreas @ kritzelkratz.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/
 
using UnityEngine;
using DashImporter;

public class ImportTest : MonoBehaviour
{
    private Importer _importer;

    private bool loading = false;

    private float startTime;

    private GameObject _loadedObject = null;
    private int _animClipIndex = 0;

    void Start()
    {
        startTime = Time.realtimeSinceStartup;

        string path = Application.streamingAssetsPath + "/mini_knight.dmf";

        _importer = new Importer();

        _importer.Import(path, OnLoadingFinsihed);

        loading = true;
    }

    void Update()
    {
        // loading animation
        if (loading == true)
        {
            transform.Rotate(new Vector3(0f, 0f, 360f * Time.deltaTime));
        }

        if (_loadedObject != null)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                Animation anim = _loadedObject.GetComponent<Animation>();

                int clipCount = anim.GetClipCount();

                if (clipCount > 0)
                {
                    string animName = GetClipNameByIndex(anim, _animClipIndex);
                    anim.Play(animName);

                    _animClipIndex++;

                    if (_animClipIndex >= clipCount)
                    {
                        _animClipIndex = 0;
                    }
                }
            }
        }

    }

    private string GetClipNameByIndex(Animation anim, int index)
    {
        int count = 0;

        foreach (AnimationState state in anim)
        {
            if (count == index)
            {
                return state.name;
            }
            count++;
        }

        return "";
    }

    private void OnLoadingFinsihed(GameObject obj)
    {
        //        Debug.Log("loading finished");

        loading = false;

        _loadedObject = obj;

        Renderer renderer = GetComponent<Renderer>();
        if (renderer != null)
        {
            renderer.enabled = false;
        }

        Debug.Log("Time for loading: " + (Time.realtimeSinceStartup - startTime));
    }

    void OnApplicationQuit()
    {
        _importer.CancelImport();

        Debug.Log("Application ending after " + Time.time + " seconds");
    }
}
