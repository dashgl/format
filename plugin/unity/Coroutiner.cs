/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019 Andreas Scholl (andreas @ kritzelkratz.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/

using UnityEngine;
using System.Collections;

public class Coroutiner : MonoBehaviour
{
	private static Coroutiner _instance;

	public static Coroutiner Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = UnityEngine.Object.FindObjectOfType<Coroutiner>() as Coroutiner;

                if (_instance == null)
                {
                    GameObject coroutiner = new GameObject("Coroutiner");

                    _instance = coroutiner.AddComponent<Coroutiner>();

            }
			}
			return _instance;
		}
	}

    public Coroutine StartNonMonoCoroutine(IEnumerator firstIterationResult)
	{
        return StartCoroutine( firstIterationResult );
    }

	public void OnApplicationQuit()
	{
    	_instance = null;
	}
}
