/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019 Andreas Scholl (andreas @ kritzelkratz.de)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/
 
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DashGL;

public class MeshCreator
{
    public struct VertexData : IEquatable<VertexData>
    {
        public Vector3 Vertex;
        public Vector3 Normal;
        public Vector4 Tangent;
        public Vector2 uv1;
        public Vector2 uv2;
        public int material;

        public bool Equals(VertexData other)
        {
            return (Vertex == other.Vertex &&
                    Normal == other.Normal &&
                    Tangent == other.Tangent &&
                    uv1 == other.uv1 &&
                    uv2 == other.uv2 &&
                    material == other.material);
        }

        //based on algorithm by Joshua Bloch from "Effective Java" page 48
        public override int GetHashCode()
        {
            int hash = 42;
            hash = 23 * hash + Vertex.GetHashCode();
            hash = 23 * hash + Normal.GetHashCode();
            hash = 23 * hash + Tangent.GetHashCode();
            hash = 23 * hash + uv1.GetHashCode();
            hash = 23 * hash + uv2.GetHashCode();
            hash = 23 * hash + material.GetHashCode();
            return hash;
        }
    }

    Vector3[] _vertexData;
    Vector3[] _normalData;
    Vector2[] _uv1Data;
    int[] _indexData;
    int[] _materialData;

    public Mesh CreateMesh(DashLoader loader)
    {
        List<Vector3> vertices = loader.GetVertices();
        List<VertexBoneAttributes> boneWeights = loader.GetVertexBoneAttributes();
        List<Vector3> normals = loader.GetNormals();
        int[] indices = loader.GetIndices();
        List<Vector2> uv1s = loader.GetUvs();
        List<int> materials = loader.GetMaterialIds();
        int materialCount = loader.GetMaterialCount();

        Mesh mesh = new Mesh();

        if (indices.Length <= 65535)
        {
            mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt16;
        }
        else
        {
            mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        }

        List<Vector3> vertexList = new List<Vector3>();
        List<BoneWeight> boneWeightList = new List<BoneWeight>();
        List<int> indexList = new List<int>();

        for (int count = 0; count < indices.Length; count++)
        {
            vertexList.Add(vertices[indices[count]]);

            // compose unity bone weight
            VertexBoneAttributes boneAttribute = boneWeights[indices[count]];
            BoneWeight weight = new BoneWeight();
            weight.boneIndex0 = (int)boneAttribute.index1;
            weight.weight0 = boneAttribute.weight1;
            weight.boneIndex1 = (int)boneAttribute.index2;
            weight.weight1 = boneAttribute.weight2;
            weight.boneIndex2 = (int)boneAttribute.index3;
            weight.weight2 = boneAttribute.weight3;
            weight.boneIndex3 = (int)boneAttribute.index4;
            weight.weight3 = boneAttribute.weight4;
            boneWeightList.Add(weight);

            indexList.Add(count);
        }
        _indexData = indexList.ToArray();

        _vertexData = vertexList.ToArray();
        _normalData = normals.ToArray();
        _uv1Data = uv1s.ToArray();
        _materialData = materials.ToArray();    // per face materials

        //        PrepareVertexNormals();

        mesh.SetVertices(new List<Vector3>(_vertexData));
        mesh.boneWeights = boneWeightList.ToArray();
        mesh.SetNormals(new List<Vector3>(_normalData));
        //        mesh.SetIndices(_indexData, MeshTopology.Triangles, 0);
        mesh.SetUVs(0, new List<Vector2>(_uv1Data));

        // build submesh index lists
        //
        List<List<int>> subMeshes = new List<List<int>>();

        for (int count = 0; count < materialCount; count++)
        {
            subMeshes.Add(new List<int>());
        }

        for (int indexCount = 0; indexCount < _indexData.Length; indexCount += 3)
        {
//            int material = _materialData[_indexData[indexCount]];
            int material = _materialData[indexCount / 3];

            subMeshes[material].Add(_indexData[indexCount]);
            subMeshes[material].Add(_indexData[indexCount + 1]);
            subMeshes[material].Add(_indexData[indexCount + 2]);
        }

        mesh.subMeshCount = subMeshes.Count;

        for (int subMeshCount = 0; subMeshCount < subMeshes.Count; subMeshCount++)
        {
            mesh.SetIndices(subMeshes[subMeshCount].ToArray(), MeshTopology.Triangles, subMeshCount);
        }

        mesh.name = "mesh";

        NormalSolver.RecalculateNormals(mesh, 60f);

        return mesh;
    }

    // convert polygon normals to vertex normals plus avoid duplicate vertex-data
    public void PrepareVertexNormals()
    {
        if (_normalData.Length == 0)
        {
            return;
        }

        List<int> indices = new List<int>();

        Dictionary<VertexData, int> dictVertex = new Dictionary<VertexData, int>();

        for (int count = 0; count < _vertexData.Length; count++)
        {
            Vector3 vertex = _vertexData[count];
            Vector3 normal = _normalData[count];

            Vector2 uv1 = GetUv1(count);
            Vector2 uv2 = GetUv2(count);

            int material = GetMaterial(count / 3);

            int index = StoreVertex(dictVertex, vertex, normal, uv1, uv2, material);
            indices.Add(index);
        }

        // dictonary to arrays
        var keys = dictVertex.Keys.ToList();
        int keyCount = dictVertex.Count;

        _vertexData = new Vector3[keyCount];
        _normalData = new Vector3[keyCount];
        _uv1Data = new Vector2[keyCount];
//        Uv2Data = new Vector2[keyCount];
        _materialData = new int[keyCount];

        for (int i = 0; i < keyCount; i++)
        {
            _vertexData[i] = keys[i].Vertex;
            _normalData[i] = keys[i].Normal;
            _uv1Data[i] = keys[i].uv1;
//            Uv2Data[i] = keys[i].uv2;
            _materialData[i] = keys[i].material;
        }

        _indexData = indices.ToArray();
    }

    private int StoreVertex(Dictionary<VertexData, int> dictVertex, Vector3 vertex, Vector3 normal, Vector2 uv1, Vector2 uv2, int material)
    {
        VertexData data = new VertexData
        {
            Vertex = vertex,
            Tangent = Vector3.zero,
            Normal = normal,
            uv1 = uv1,
            uv2 = uv2,
            material = material
        };

        if (dictVertex.ContainsKey(data) == false)
        {
            dictVertex[data] = dictVertex.Count;
        }

        return dictVertex[data];
    }

    private Vector2 GetUv1(int index)
    {
        Vector2 uv = Vector2.zero;

        if (_uv1Data != null)
        {
            if (index < _uv1Data.Length)
            {
                uv = _uv1Data[index];
            }
        }

        return uv;
    }


    private Vector2 GetUv2(int index)
    {
        Vector2 uv = Vector2.zero;
/*
        if (Uv2Data != null)
        {
            if (index < Uv2Data.Length)
            {
                uv = Uv2Data[index];
            }
        }
*/
        return uv;
    }

    private int GetMaterial(int index)
    {
        int material = 0;

        if (_materialData != null)
        {
            if (index < _materialData.Length)
            {
                material = _materialData[index];
            }
        }

        return material;
    }
}

/*

        mesh.SetNormals(new List<Vector3>(md.NormalData));

        //for now to just be able to put these v3s in a list auf v4s
        List<Vector4> tangentList = new List<Vector4>();
        if (md.TangentData != null)
        {
            foreach (Vector4 v in md.TangentData)
            {
                tangentList.Add(new Vector4(v.x, v.y, v.z, v.w));
            }
        }

        mesh.SetUVs(0, new List<Vector2>(md.Uv1Data));
        mesh.SetUVs(1, new List<Vector2>(md.Uv2Data));

        if (tangentList.Count != 0)
        {
            mesh.SetTangents(tangentList);
        }
*/
