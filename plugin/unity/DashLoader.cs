using UnityEngine;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System;

namespace DashGL
{
    public struct ImageAttributes
    {
        public int id;
        public uint flipY;
        public uint width;
        public uint height;
        public uint wrapS;
        public uint wrapT;
    }

    public struct MaterialAttributes
    {
        public string name;
        public int id;
        public int textureId;

        public uint blendingMode;
        public uint blendingEquation;
        public uint blendingSrc;
        public uint blendingDst;

        public uint skinning;
        public uint shader_type;

        public uint side;
        public uint shadowSide;

        public uint light;
        public uint vertexColors;

        public uint use_alpha;
        public uint skip_render;

        public float alphaTest;

        public Color diffuse;
        public Color emissive;
        public float emissiveCoef;
        public Color specular;
        public float specularCoef;
    }

    public struct KeyAttributes
    {
        public float time;
        public Vector3 pos;
        public Vector4 rot;
        public Vector3 scl;
        public bool usePos;
        public bool useRot;
        public bool useScl;
    }

    public struct HierarchyAttributes
    {
        public int parent;
        public List<KeyAttributes> keys;
    }

    public struct AnimationAttributes
    {
        public string name;
        public int id;
        public int fps;
        public float length;
        public List<HierarchyAttributes> hierarchy;
    }

    public struct BoneAttributes
    {
        public string name;

        public int id;
        public int parentId;

        public Matrix4x4 matrix;
    }

    public struct VertexBoneAttributes
    {
        public uint index1;
        public uint index2;
        public uint index3;
        public uint index4;
        public float weight1;
        public float weight2;
        public float weight3;
        public float weight4;
    }

    public class DashLoader
    {
        private uint _version;
        private uint _sub_version;

        private BinaryReader _reader = null;

        private List<int> _materialIds;
        private int _materialCount;
        private List<Vector3> _vertices;
        private List<VertexBoneAttributes> _vertexBoneAttributes;
        private List<int> _indices;
        private List<Vector3> _normals;
        private List<Color> _vertexColors;
        private List<uint> _alphas;
        private List<Vector2> _uvs;
        private List<Byte[]> _textures;
        private List<ImageAttributes> _textureAttributes;
        private List<MaterialAttributes> _materialAttributes;
        private List<BoneAttributes> _boneAttributes;
        private List<AnimationAttributes> _animationAttributes;

        public DashLoader()
        {
        }

        public bool LoadAndParse(string filePath)
        {
            byte[] byteArray = File.ReadAllBytes(filePath);

            _reader = new BinaryReader(new MemoryStream(byteArray));

            Parse();

            _reader.Close();

            return true;
        }

        public int GetMaterialCount()
        {
            return _materialCount;
        }

        public List<Vector3> GetVertices()
        {
            return _vertices;
        }

        public List<VertexBoneAttributes> GetVertexBoneAttributes()
        {
            return _vertexBoneAttributes;
        }

        public int[] GetIndices()
        {
            return _indices.ToArray();
        }

        public List<Vector3> GetNormals()
        {
            return _normals;
        }

        public List<Color> GetVertexColors()
        {
            return _vertexColors;
        }

        public List<Vector2> GetUvs()
        {
            return _uvs;
        }

        public List<int> GetMaterialIds()
        {
            return _materialIds;
        }

        public List<Byte[]> GetTextures()
        {
            return _textures;
        }

        public List<ImageAttributes> GetTextureAttributes()
        {
            return _textureAttributes;
        }

        public List<MaterialAttributes> GetMaterialAttributes()
        {
            return _materialAttributes;
        }

        public List<AnimationAttributes> GetAnimationAttributes()
        {
            return _animationAttributes;
        }

        public List<BoneAttributes> GetBoneAttributes()
        {
            return _boneAttributes;
        }

        private uint getUint32(uint pos)
        {
            _reader.BaseStream.Seek(pos, SeekOrigin.Begin);
            uint value = _reader.ReadUInt32();
            return value;
        }

        private byte getByte(uint pos)
        {
            _reader.BaseStream.Seek(pos, SeekOrigin.Begin);
            byte value = _reader.ReadByte();
            return value;
        }

        private byte[] getBytes(uint pos, int count)
        {
            _reader.BaseStream.Seek(pos, SeekOrigin.Begin);
            byte[] array = _reader.ReadBytes(count);
            return array;
        }

        private uint getUint8(uint pos)
        {
            _reader.BaseStream.Seek(pos, SeekOrigin.Begin);
            uint value = _reader.ReadByte();
            return value;
        }

        private char getChar(uint pos)
        {
            _reader.BaseStream.Seek(pos, SeekOrigin.Begin);
            char value = _reader.ReadChar();
            return value;
        }

        private uint getUint16(uint pos)
        {
            _reader.BaseStream.Seek(pos, SeekOrigin.Begin);
            uint value = _reader.ReadUInt16();
            return value;
        }

        private int getInt16(uint pos)
        {
            _reader.BaseStream.Seek(pos, SeekOrigin.Begin);
            int value = _reader.ReadInt16();
            return value;
        }

        private float getFloat32(uint pos)
        {
            _reader.BaseStream.Seek(pos, SeekOrigin.Begin);
            float value = _reader.ReadSingle();
            return value;
        }

        private string getString(uint ofs, int maxChars)
        {
            StringBuilder builder = new StringBuilder();

            for (uint k = 0; k < maxChars; k++)
            {
                char character = getChar(ofs + k);
                if (character == 0)
                {
                    break;
                }
                builder.Append(character);
            }

            return builder.ToString();
        }

        private bool Parse()
        {
            uint magic = getUint32(0x00);
            _version = getUint16(0x04);
            _sub_version = getUint16(0x06);

            // dash format?
            if (magic != DashConstants.DASH_MAGIC_NUMBER)
            {
                return false;
            }

            // correct version?
            if ((_version != DashConstants.DASH_MAJOR_VERSION) /*&& (_sub_version != DashConstants.DASH_MINOR_VERSION)*/)
            {
                return false;
            }

            // Parse Header
            //
            uint texturesOffset = getUint32(0x18);
            uint texturesCount = getUint32(0x1c);

            uint materialsOffset = getUint32(0x28);
            uint materialsCount = getUint32(0x2c);

            uint verticesOffset = getUint32(0x38);
            uint verticesCount = getUint32(0x3c);

            uint facesOffset = getUint32(0x48);
            uint facesCount = getUint32(0x4c);

            uint bonesOffset = getUint32(0x58);
            uint bonesCount = getUint32(0x5c);

            uint animationsOffset = getUint32(0x68);
            uint animationsCount = getUint32(0x6c);

            // prepare data lists
            //
            _materialIds = new List<int>();
            _vertices = new List<Vector3>();
            _vertexBoneAttributes = new List<VertexBoneAttributes>();
            _indices = new List<int>();
            _normals = new List<Vector3>();
            _vertexColors = new List<Color>();
            _alphas = new List<uint>();
            _uvs = new List<Vector2>();

            _textures = new List<Byte[]>();
            _textureAttributes = new List<ImageAttributes>();
            _materialAttributes = new List<MaterialAttributes>();

            _boneAttributes = new List<BoneAttributes>();
            _animationAttributes = new List<AnimationAttributes>();

            // read textures and materials
            //
            readTextures(texturesOffset, texturesCount);
            readMaterials(materialsOffset, materialsCount);

            // read geometry
            //
            readVertices(verticesOffset, verticesCount);

            if (_sub_version < 0x05)
            {
                readFacesOld(facesOffset, facesCount);
            }
            else
            {
                readFaces(facesOffset, facesCount);
            }

            // read bones and animations
            readBones(bonesOffset, bonesCount);
            readAnimations(animationsOffset, animationsCount);

            return true;
        }

        private void readVertices(uint ofs, uint count)
        {
            if (ofs == 0 || count == 0)
            {
                return;
            }

            for (int index = 0; index < count; index++)
            {
                Vector3 pos = new Vector3();

                pos.x = getFloat32(ofs + 0x00);
                pos.y = getFloat32(ofs + 0x04);
                pos.z = getFloat32(ofs + 0x08);

                _vertices.Add(pos);

                VertexBoneAttributes boneWeights = new VertexBoneAttributes();

                boneWeights.index1 = getUint16(ofs + 0x0c);
                boneWeights.index2 = getUint16(ofs + 0x0e);
                boneWeights.index3 = getUint16(ofs + 0x10);
                boneWeights.index4 = getUint16(ofs + 0x12);

                boneWeights.weight1 = getFloat32(ofs + 0x14);
                boneWeights.weight2 = getFloat32(ofs + 0x18);
                boneWeights.weight3 = getFloat32(ofs + 0x1c);
                boneWeights.weight4 = getFloat32(ofs + 0x20);

                _vertexBoneAttributes.Add(boneWeights);

                ofs += 0x24;
            }
        }

        private void readFaces(uint ofs, uint count)
        {
            if (ofs == 0 || count == 0)
            {
                return;
            }

            _materialCount = 0;

            for (int index = 0; index < count; index++)
            {
                int matId = getInt16(ofs + 0x00);
                _materialIds.Add(matId);

                if (matId >= _materialCount)
                {
                    _materialCount = matId + 1; // store number of material ids
                }

                // face indices
                //
                int a = (int)getUint32(ofs + 0x04);
                int b = (int)getUint32(ofs + 0x08);
                int c = (int)getUint32(ofs + 0x0c);

                _indices.Add(a);
                _indices.Add(b);
                _indices.Add(c);

                Vector3 aNrm = new Vector3(getFloat32(ofs + 0x1c),
                                           getFloat32(ofs + 0x20),
                                           getFloat32(ofs + 0x24));

                Vector3 bNrm = new Vector3(getFloat32(ofs + 0x28),
                                           getFloat32(ofs + 0x2c),
                                           getFloat32(ofs + 0x30));

                Vector3 cNrm = new Vector3(getFloat32(ofs + 0x34),
                                           getFloat32(ofs + 0x38),
                                           getFloat32(ofs + 0x3c));

                _normals.Add(aNrm);
                _normals.Add(bNrm);
                _normals.Add(cNrm);

                // vertex colors
                Color aClr = new Color((float)getUint8(ofs + 0x10) / 255f,
                                       (float)getUint8(ofs + 0x11) / 255f,
                                       (float)getUint8(ofs + 0x12) / 255f);

                uint aAlpha = getUint8(ofs + 0x13);

                Color bClr = new Color((float)getUint8(ofs + 0x14) / 255f,
                                       (float)getUint8(ofs + 0x15) / 255f,
                                       (float)getUint8(ofs + 0x16) / 255f);

                uint bAlpha = getUint8(ofs + 0x17);

                Color cClr = new Color((float)getUint8(ofs + 0x18) / 255f,
                                       (float)getUint8(ofs + 0x19) / 255f,
                                       (float)getUint8(ofs + 0x1a) / 255f);

                uint cAlpha = getUint8(ofs + 0x1b);

                _vertexColors.Add(aClr);
                _vertexColors.Add(bClr);
                _vertexColors.Add(cClr);

                _alphas.Add(aAlpha);
                _alphas.Add(bAlpha);
                _alphas.Add(cAlpha);

                Vector2 aUV = new Vector2(getFloat32(ofs + 0x40),
                                          getFloat32(ofs + 0x44));

                Vector2 bUV = new Vector2(getFloat32(ofs + 0x48),
                                          getFloat32(ofs + 0x4c));
           
                Vector2 cUV = new Vector2(getFloat32(ofs + 0x50),
                                          getFloat32(ofs + 0x54));

                _uvs.Add(aUV);
                _uvs.Add(bUV);
                _uvs.Add(cUV);

                ofs += 0x58;
            }
        }

        private void readFacesOld(uint ofs, uint count)
        {
            if (ofs == 0 || count == 0)
            {
                return;
            }

            _materialCount = 0;

            for (int index = 0; index < count; index++)
            {
                int matId = getInt16(ofs + 0x00);
                _materialIds.Add(matId);

                if (matId >= _materialCount)
                {
                    _materialCount = matId + 1; // store number of material ids
                }

                // face indices
                //
                int a = (int)getUint16(ofs + 0x02);
                int b = (int)getUint16(ofs + 0x04);
                int c = (int)getUint16(ofs + 0x06);

                _indices.Add(a);
                _indices.Add(b);
                _indices.Add(c);

                Vector3 aNrm = new Vector3(getFloat32(ofs + 0x14),
                                           getFloat32(ofs + 0x18),
                                           getFloat32(ofs + 0x1c));

                Vector3 bNrm = new Vector3(getFloat32(ofs + 0x20),
                                           getFloat32(ofs + 0x24),
                                           getFloat32(ofs + 0x28));

                Vector3 cNrm = new Vector3(getFloat32(ofs + 0x2c),
                                           getFloat32(ofs + 0x30),
                                           getFloat32(ofs + 0x34));

                _normals.Add(aNrm);
                _normals.Add(bNrm);
                _normals.Add(cNrm);

                // vertex colors
                Color aClr = new Color((float)getUint8(ofs + 0x08) / 255f,
                                       (float)getUint8(ofs + 0x09) / 255f,
                                       (float)getUint8(ofs + 0x0a) / 255f);

                uint aAlpha = getUint8(ofs + 0x0b);

                Color bClr = new Color((float)getUint8(ofs + 0x0c) / 255f,
                                       (float)getUint8(ofs + 0x0d) / 255f,
                                       (float)getUint8(ofs + 0x0e) / 255f);

                uint bAlpha = getUint8(ofs + 0x0f);

                Color cClr = new Color((float)getUint8(ofs + 0x10) / 255f,
                                       (float)getUint8(ofs + 0x11) / 255f,
                                       (float)getUint8(ofs + 0x12) / 255f);

                uint cAlpha = getUint8(ofs + 0x13);

                _vertexColors.Add(aClr);
                _vertexColors.Add(bClr);
                _vertexColors.Add(cClr);

                _alphas.Add(aAlpha);
                _alphas.Add(bAlpha);
                _alphas.Add(cAlpha);

                Vector2 aUV = new Vector2(getFloat32(ofs + 0x38),
                                          getFloat32(ofs + 0x3c));

                Vector2 bUV = new Vector2(getFloat32(ofs + 0x40),
                                          getFloat32(ofs + 0x44));

                Vector2 cUV = new Vector2(getFloat32(ofs + 0x48),
                                          getFloat32(ofs + 0x4c));

                _uvs.Add(aUV);
                _uvs.Add(bUV);
                _uvs.Add(cUV);

                ofs += 0x50;
            }
        }



        private void readTextures(uint ofs, uint count)
        {
            for (int index = 0; index < count; index++)
            {
                String name = getString(ofs, 0x20);

                ImageAttributes img = new ImageAttributes();

                img.id = getInt16(ofs + 0x20);
                img.flipY = getUint16(ofs + 0x22);
                uint img_offset = getUint32(ofs + 0x24);
                uint img_length = getUint32(ofs + 0x28);
                img.width = getUint16(ofs + 0x2c);
                img.height = getUint16(ofs + 0x2e);
                img.wrapS = getUint16(ofs + 0x30);
                img.wrapT = getUint16(ofs + 0x32);

                // image data as binary png
                //
                byte[] array = getBytes(img_offset, (int)img_length);
                _textures.Add(array);
                _textureAttributes.Add(img);

                ofs += 0x40;
            }
        }

        private void readMaterials(uint ofs, uint count)
        {
            for (int index = 0; index < count; index++)
            {
                MaterialAttributes mat = new MaterialAttributes();

                mat.name = getString(ofs, 0x20);

                mat.id = getInt16(ofs + 0x20);
                mat.textureId = getInt16(ofs + 0x22);
                mat.blendingMode = getUint16(ofs + 0x24);
                mat.blendingEquation = getUint16(ofs + 0x26);
                mat.blendingSrc = getUint16(ofs + 0x28);
                mat.blendingDst = getUint16(ofs + 0x2a);

                mat.skinning = getUint16(ofs + 0x2c);
                mat.shader_type = getUint16(ofs + 0x2e);

                mat.side = getUint16(ofs + 0x30);
                mat.shadowSide = getUint16(ofs + 0x32);

                mat.light = getUint16(ofs + 0x34);
                mat.vertexColors = getUint16(ofs + 0x36);

                mat.use_alpha = getUint16(ofs + 0x38);
                mat.skip_render = getUint16(ofs + 0x3a);

                mat.alphaTest = getFloat32(ofs + 0x3c);

                mat.diffuse = new Color(getFloat32(ofs + 0x40),
                                        getFloat32(ofs + 0x44),
                                        getFloat32(ofs + 0x48),
                                        getFloat32(ofs + 0x4c));

                mat.emissive = new Color(getFloat32(ofs + 0x50),
                                        getFloat32(ofs + 0x54),
                                        getFloat32(ofs + 0x58));
                mat.emissiveCoef = getFloat32(ofs + 0x5c);

                mat.specular = new Color(getFloat32(ofs + 0x60),
                                        getFloat32(ofs + 0x64),
                                        getFloat32(ofs + 0x68));
                mat.specularCoef = getFloat32(ofs + 0x6c);

                _materialAttributes.Add(mat);

                ofs += 0x70;
            }
        }

        private void readBones(uint ofs, uint count)
        {
            for (int index = 0; index < count; index++)
            {
                BoneAttributes bone = new BoneAttributes();

                bone.name = getString(ofs, 0x20);

                bone.id = getInt16(ofs + 0x20);
                bone.parentId = getInt16(ofs + 0x22);

                Matrix4x4 matrix = new Matrix4x4();
                matrix[0] = getFloat32(ofs + 0x30);
                matrix[1] = getFloat32(ofs + 0x34);
                matrix[2] = getFloat32(ofs + 0x38);
                matrix[3] = getFloat32(ofs + 0x3c);
                matrix[4] = getFloat32(ofs + 0x40);
                matrix[5] = getFloat32(ofs + 0x44);
                matrix[6] = getFloat32(ofs + 0x48);
                matrix[7] = getFloat32(ofs + 0x4c);
                matrix[8] = getFloat32(ofs + 0x50);
                matrix[9] = getFloat32(ofs + 0x54);
                matrix[10] = getFloat32(ofs + 0x58);
                matrix[11] = getFloat32(ofs + 0x5c);
                matrix[12] = getFloat32(ofs + 0x60);
                matrix[13] = getFloat32(ofs + 0x64);
                matrix[14] = getFloat32(ofs + 0x68);
                matrix[15] = getFloat32(ofs + 0x6c);

                bone.matrix = matrix;

                ofs += 0x70;

                _boneAttributes.Add(bone);
            }
        }

        private void readAnimations(uint ofs, uint count)
        {
            for (int index = 0; index < count; index++)
            {
                AnimationAttributes animation = new AnimationAttributes();

                animation.name = getString(ofs, 0x20);

                animation.id = getInt16(ofs + 0x20);
                animation.fps = getInt16(ofs + 0x22);
                uint offset = getUint32(ofs + 0x24);
                uint animCount = getUint32(ofs + 0x28);
                animation.length = getFloat32(ofs + 0x2c);

                animation.hierarchy = new List<HierarchyAttributes>();

                ofs += 0x30;

                for (int k = 0; k < animCount; k++)
                {
                    int bone = getInt16(offset + 0x00);
                    uint flags = getUint16(offset + 0x02);
                    float time = getFloat32(offset + 0x04);
                    Vector3 pos = new Vector3(getFloat32(offset + 0x08),
                                            getFloat32(offset + 0x0c),
                                            getFloat32(offset + 0x10));
                    Vector4 rot = new Vector4(getFloat32(offset + 0x14),
                                            getFloat32(offset + 0x18),
                                            getFloat32(offset + 0x1c),
                                            getFloat32(offset + 0x20));
                    Vector3 scl = new Vector3(getFloat32(offset + 0x24),
                                            getFloat32(offset + 0x28),
                                            getFloat32(offset + 0x2c));

                    offset += 0x30;

                    if (animation.hierarchy.Count <= bone)
                    {
                        HierarchyAttributes hierarchy = new HierarchyAttributes();
                        hierarchy.parent = bone;
                        hierarchy.keys = new List<KeyAttributes>();
                        animation.hierarchy.Add(hierarchy);
                    }

                    KeyAttributes key = new KeyAttributes();

                    key.time = time;

                    if ((flags & DashConstants.DASH_ANIMATION_POS) != 0)
                    {
                        key.pos = pos;
                        key.usePos = true;
                    }
                    else
                    {
                        key.usePos = false;
                    }

                    if ((flags & DashConstants.DASH_ANIMATION_ROT) != 0)
                    {
                        key.rot = rot;
                        key.useRot = true;
                    }
                    else
                    {
                        key.useRot = false;
                    }

                    if ((flags & DashConstants.DASH_ANIMATION_SCL) != 0)
                    {
                        key.scl = scl;
                        key.useScl = true;
                    }
                    else
                    {
                        key.useScl = false;
                    }

                    animation.hierarchy[bone].keys.Add(key);
                }

                _animationAttributes.Add(animation);
            }
        }
    }
}