/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019,2020 Benjamin Collins (kion @ dashgl.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/

#include <fbxsdk.h>
#include "../Common/Common.h"
#include "dash_types.h"

char input_file[FILENAME_MAX];
char output_file[FILENAME_MAX];

bool CreateScene(FbxManager* pSdkManager, FbxScene* pScene);
FbxNode* CreateMesh(FbxScene* pScene, const char* pName);
FbxNode* CreateSkeleton(FbxScene* pScene, FbxNode* pMesh, const char* pName);

int main(int argc, char* argv[]) {

	FbxManager* lSdkManager = NULL;
	FbxScene* lScene = NULL;
	bool lResult;

	/**
	 * CHECK Command line arguments
	 **/

	if(argc < 2 || argc > 3) {
		fprintf(stderr, "Usage: ./dmf_conv [in file] [out file(optional]\n");
		exit(1);
	} else if(argc == 2) {
		strcpy(input_file, argv[1]);
		strcpy(output_file, argv[1]);
		char *dot = strrchr(output_file, '.');
		*dot = 0;
		strcat(dot, ".fbx\0");
	} else if(argc == 3) {
		strcpy(input_file, argv[1]);
		strcpy(output_file, argv[2]);
	}

	// Prepare the FBX SDK
	InitializeSdkObjects(lSdkManager, lScene);

	// Create the scene
	lResult = CreateScene(lSdkManager, lScene);
	if(lResult == false) {
		FBXSDK_printf("\n\n100 An error occurred while creating the scene...\n");
		DestroySdkObjects(lSdkManager, lResult);
		return 0;
	}

	// Export the file

	int lFileFormat = lSdkManager->GetIOPluginRegistry()->GetNativeWriterFormat();
	lResult = SaveScene(lSdkManager, lScene, output_file, lFileFormat, true);
	if(lResult == false) {
		FBXSDK_printf("\n\n200 An error occurred while creating the scene...\n");
		DestroySdkObjects(lSdkManager, lResult);
		return 0;
	}

	DestroySdkObjects(lSdkManager, lResult);
	FBXSDK_printf("\n\nOkay, here we go!!!!\n");
	printf("Dest file: %s\n", output_file);
	return 0;

}

bool CreateScene(FbxManager *pSdkManager, FbxScene* pScene) {

	FBXSDK_printf("--- Create Scene ---\n");

	// Create scene info

	FbxDocumentInfo* sceneInfo = FbxDocumentInfo::Create(pSdkManager,"SceneInfo");
	sceneInfo->mTitle = "Example scene";
	sceneInfo->mSubject = "Illustrates the creation and animation of a deformed cylinder.";
	sceneInfo->mAuthor = "ExportScene01.exe sample program.";
	sceneInfo->mRevision = "rev. 1.0";
	sceneInfo->mKeywords = "deformed cylinder";
	sceneInfo->mComment = "no particular comments required.";
	pScene->SetSceneInfo(sceneInfo);
	
	// Create Mesh

	FbxNode* lMesh = CreateMesh(pScene, "Mesh");
	FbxNode* lSkeleton = CreateSkeleton(pScene, lMesh, "Skeleton");

	// Build Node Tree

	FbxNode* lRootNode = pScene->GetRootNode();
	lRootNode->AddChild(lMesh);
	lRootNode->AddChild(lSkeleton);

	return true;

}

FbxNode* CreateMesh(FbxScene* pScene, const char* pName) {

	FBXSDK_printf("--- Create Mesh ---\n");
	
	FbxMesh* lMesh = FbxMesh::Create(pScene,pName);
	FbxNode* lNode = FbxNode::Create(pScene,pName);
	lNode->SetNodeAttribute(lMesh);

	FILE *fp, *wp;
	fp = fopen(input_file, "rb");
	if(fp == NULL) {
		return lNode;
	}

	FBXSDK_printf("--- Dash Read Header ---\n");
	
	int i, pos;
	uint8_t *img;
	DashMagic_t dMagic;
	DashMaterial_t dMat;
	DashTexture_t dTex;
	DashHeader_t dHead;
	DashVertex_t dVert;
	DashFace_t dFace;

	fread(&dMagic, sizeof(DashMagic_t), 1, fp);
	fread(&dHead, sizeof(DashHeader_t), 1, fp);

	printf("Reading Dash Model Format\n");
	printf("Version: %d\n", dMagic.major_version);
	printf("Sub Version: %d\n", dMagic.minor_version);
	putchar('\n');

	if(dMagic.magic != DASH_MAGIC_NUMBER) {
		return lNode;
	}

	if(dMagic.major_version != DASH_MAJOR_VERSION) {
		return lNode;
	}

	if(dMagic.minor_version != DASH_MINOR_VERSION) {
		return lNode;
	}

	FBXSDK_printf("--- Read Textures ---\n");

	FbxFileTexture** lTextureList = (FbxFileTexture**)malloc(dHead.tex.count * sizeof(FbxFileTexture*));

	fseek(fp, dHead.tex.offset, SEEK_SET);
	for(i = 0; i < dHead.tex.count; i++) {
		
		fread(&dTex, sizeof(DashTexture_t), 1, fp);
		pos = ftell(fp);

		printf("Texture name: %s\n", dTex.name);
		fseek(fp, dTex.img_offset, SEEK_SET);
		img = (uint8_t*)malloc(dTex.img_length);
		fread(img, dTex.img_length, 1, fp);
		fseek(fp, pos, SEEK_SET);

		strcat(dTex.name, ".png");
		wp = fopen(dTex.name, "wb");
		fwrite(img, dTex.img_length, 1, wp);
		fclose(wp);
		free(img);

		lTextureList[i] = FbxFileTexture::Create(pScene,dTex.name);
		lTextureList[i]->SetFileName(dTex.name);
		lTextureList[i]->SetTextureUse(FbxTexture::eStandard);
		lTextureList[i]->SetMappingType(FbxTexture::eUV);
		lTextureList[i]->SetMaterialUse(FbxFileTexture::eModelMaterial);
		lTextureList[i]->SetSwapUV(false);
		lTextureList[i]->SetTranslation(0.0, 0.0);
		lTextureList[i]->SetScale(1.0, 1.0);
		lTextureList[i]->SetRotation(0.0, 0.0);
		lTextureList[i]->UVSet.Set(FbxString("DiffuseUV"));

	}

	FBXSDK_printf("--- Read Materials ---\n");
	
	FbxLayerElementMaterial* lLayerElementMaterial = lMesh->CreateElementMaterial();
	lLayerElementMaterial->SetMappingMode(FbxGeometryElement::eByPolygon);
	lLayerElementMaterial->SetReferenceMode(FbxLayerElement::eIndexToDirect);

	int16_t tex_list[dHead.mat.count];
	fseek(fp, dHead.mat.offset, SEEK_SET);
	for(i = 0; i < dHead.mat.count; i++) {
		fread(&dMat, sizeof(DashMaterial_t), 1, fp);
		FbxSurfacePhong *lMaterial = FbxSurfacePhong::Create(pScene, dMat.name);
		FbxDouble3 lDiffuse(dMat.diffuse[0],dMat.diffuse[1],dMat.diffuse[2]);
		FbxDouble3 lEmissive(dMat.emissive[0],dMat.emissive[1],dMat.emissive[2]);

		lMaterial->Emissive.Set(lEmissive);
		lMaterial->Diffuse.Set(lDiffuse);
		lMaterial->TransparencyFactor.Set(0.0);
		lMaterial->ShadingModel.Set("Phong");
		lMaterial->Shininess.Set(0.5);
		lNode->AddMaterial(lMaterial);

		printf("Texture Id: %d\n", dMat.tex_id);
		
		tex_list[i] = dMat.tex_id;
		if(dMat.tex_id != -1) {
			lMaterial->Diffuse.ConnectSrcObject(lTextureList[dMat.tex_id]);
		}

	}

	free(lTextureList);

	FBXSDK_printf("--- Read Vertices ---\n");
	
	lMesh->InitControlPoints(dHead.vert.count);
	FbxVector4* lControlPoints = lMesh->GetControlPoints();

	printf("Vertex offset: 0x%08x\n", dHead.vert.offset);
	printf("Vertex count: %d\n", dHead.vert.count);
	
	fseek(fp, dHead.vert.offset, SEEK_SET);
	for(i = 0; i < dHead.vert.count; i++) {
		fread(&dVert, sizeof(DashVertex_t), 1, fp);
		lControlPoints[i] = FbxVector4(dVert.pos[0],dVert.pos[1],dVert.pos[2]);
	}

	FBXSDK_printf("--- Read Faces ---\n");

	// Create normals (polygonVertexMapping)

	FbxGeometryElementNormal* lGeometryElementNormal= lMesh->CreateElementNormal();
	lGeometryElementNormal->SetMappingMode(FbxGeometryElement::eByPolygonVertex);
	lGeometryElementNormal->SetReferenceMode(FbxGeometryElement::eDirect);

	// Create uv's (polygonVertexMapping)

	static const char* gDiffuseElementName = "DiffuseUV";
	FbxGeometryElementUV* lUVDiffuseElement = lMesh->CreateElementUV( gDiffuseElementName);
	FBX_ASSERT( lUVDiffuseElement != NULL);
	lUVDiffuseElement->SetMappingMode(FbxGeometryElement::eByPolygonVertex);
	lUVDiffuseElement->SetReferenceMode(FbxGeometryElement::eDirect);

	// Create vertex color (polygonVertexMapping)
	
	const double f = 255L;
	FbxGeometryElementVertexColor* lVertexColorElement = lMesh->CreateElementVertexColor();
	lVertexColorElement->SetMappingMode(FbxGeometryElement::eByPolygonVertex);
	lVertexColorElement->SetReferenceMode(FbxGeometryElement::eDirect);
	
	fseek(fp, dHead.face.offset, SEEK_SET);
	for(i = 0; i < dHead.face.count; i++) {
		
		fread(&dFace, sizeof(DashFace_t), 1, fp);

		lMesh->BeginPolygon(dFace.mat_id);
		lMesh->AddPolygon(dFace.a);
		lMesh->AddPolygon(dFace.b);
		lMesh->AddPolygon(dFace.c);
		lMesh->EndPolygon();

		// Add normals

		FbxVector4 aNorm(dFace.aNrm[0], dFace.aNrm[1],dFace.aNrm[2]);
		FbxVector4 bNorm(dFace.bNrm[0], dFace.bNrm[1],dFace.bNrm[2]);
		FbxVector4 cNorm(dFace.cNrm[0], dFace.cNrm[1],dFace.cNrm[2]);
		lGeometryElementNormal->GetDirectArray().Add(aNorm);
		lGeometryElementNormal->GetDirectArray().Add(bNorm);
		lGeometryElementNormal->GetDirectArray().Add(cNorm);
	
		// add uv's

		FbxVector2 aUv(dFace.aUv[0], dFace.aUv[1]);
		FbxVector2 bUv(dFace.bUv[0], dFace.bUv[1]);
		FbxVector2 cUv(dFace.cUv[0], dFace.cUv[1]);

		lUVDiffuseElement->GetDirectArray().Add(aUv);
		lUVDiffuseElement->GetDirectArray().Add(bUv);
		lUVDiffuseElement->GetDirectArray().Add(cUv);

		// add vertex color
		
		FbxColor aClr(dFace.aClr[0]/f,dFace.aClr[1]/f,dFace.aClr[2]/f,dFace.aClr[3]/f);
		FbxColor bClr(dFace.bClr[0]/f,dFace.bClr[1]/f,dFace.bClr[2]/f,dFace.bClr[3]/f);
		FbxColor cClr(dFace.cClr[0]/f,dFace.cClr[1]/f,dFace.cClr[2]/f,dFace.cClr[3]/f);
	}

	FBXSDK_printf("--- Return Mesh ---\n");

	fclose(fp);
	return lNode;

}

FbxNode* CreateSkeleton(FbxScene* pScene, FbxNode* pMesh, const char* pName) {

	FBXSDK_printf("--- Create Root Bone ---\n");

	FbxNode* lNode = FbxNode::Create(pScene, pName);
	FbxSkeleton* lRootBone = FbxSkeleton::Create(pScene, pName);
	lRootBone->SetSkeletonType(FbxSkeleton::eRoot);
	lNode->SetNodeAttribute(lRootBone);

	FbxPose* pPose = FbxPose::Create(pScene, "T-Pose");
	pPose->SetIsBindPose(true);
	FBXSDK_printf("--- Re-open DMF FIle ---\n");

	FILE *fp;
	fp = fopen(input_file, "rb");
	if(fp == NULL) {
		return lNode;
	}

	FBXSDK_printf("--- Dash Read Header ---\n");
	
	int i, k, pos;
	uint8_t *img;
	DashMagic_t dMagic;
	DashHeader_t dHead;
	DashBone_t dBone;
	DashVertex_t dVert;

	fread(&dMagic, sizeof(DashMagic_t), 1, fp);
	fread(&dHead, sizeof(DashHeader_t), 1, fp);
	
	FbxNode** lNodeList = (FbxNode**)malloc(dHead.bone.count * sizeof(FbxNode*));
	FbxCluster** lClusterList = (FbxCluster**)malloc(dHead.bone.count * sizeof(FbxCluster*));

	fseek(fp, dHead.bone.offset, SEEK_SET);
	for(i = 0; i < dHead.bone.count; i++) {
		fread(&dBone, sizeof(DashBone_t), 1, fp);
		float *f = dBone.matrix;

		FbxAMatrix lMatrix;
		lMatrix[0] = FbxVector4(f[0],f[1],f[2],f[3]);
		lMatrix[1] = FbxVector4(f[4],f[5],f[6],f[7]);
		lMatrix[2] = FbxVector4(f[8],f[9],f[10],f[11]);
		lMatrix[3] = FbxVector4(f[12],f[13],f[14],f[15]);

		FbxVector4 t = lMatrix.GetT();
		FbxVector4 r = lMatrix.GetR();
		FbxVector4 s = lMatrix.GetS();

		FbxSkeleton* lBone = FbxSkeleton::Create(pScene,dBone.name);
		lBone->SetSkeletonType(FbxSkeleton::eLimbNode);

		lNodeList[i] = FbxNode::Create(pScene, dBone.name);
		lNodeList[i]->SetNodeAttribute(lBone);
		lNodeList[i]->LclScaling.Set(s);
		lNodeList[i]->LclRotation.Set(r);
		lNodeList[i]->LclTranslation.Set(t);

		pPose->Add(lNodeList[i], lMatrix, true, false);
		if(i == 0) {
			lNode->AddChild(lNodeList[i]);
		} else {
			lNodeList[dBone.parent_id]->AddChild(lNodeList[i]);
		}

	}

	FbxAMatrix rootMatrix = pMesh->EvaluateGlobalTransform();

	for(i = 0; i < dHead.bone.count; i++) {

		FbxAMatrix& a = lNodeList[i]->EvaluateLocalTransform();
		FbxAMatrix& b = lNodeList[i]->EvaluateGlobalTransform();

		lClusterList[i] = FbxCluster::Create(pScene, "");
		lClusterList[i]->SetLink(lNodeList[i]);
		lClusterList[i]->SetLinkMode(FbxCluster::eTotalOne);
		lClusterList[i]->SetTransformMatrix(rootMatrix);
		lClusterList[i]->SetTransformLinkMatrix(b);

	}

	uint16_t a, b, c, d;
	float aw, bw, cw, dw;

	fseek(fp, dHead.vert.offset, SEEK_SET);
	for(i = 0; i < dHead.vert.count; i++) {
		fread(&dVert, sizeof(DashVertex_t), 1, fp);

		a = dVert.skin_index[0];
		b = dVert.skin_index[1];
		c = dVert.skin_index[2];
		d = dVert.skin_index[3];

		aw = dVert.skin_weight[0];
		bw = dVert.skin_weight[1];
		cw = dVert.skin_weight[2];
		dw = dVert.skin_weight[3];

		if(aw != 0.0f) {
			lClusterList[a]->AddControlPointIndex(i, aw);
		}

		if(bw != 0.0f) {
			lClusterList[b]->AddControlPointIndex(i, bw);
		}

		if(cw != 0.0f) {
			lClusterList[c]->AddControlPointIndex(i, cw);
		}

		if(dw != 0.0f) {
			lClusterList[d]->AddControlPointIndex(i, dw);
		}

	}

	FBXSDK_printf("--- Loop Nodes ---\n");

	for(i = 0; i < dHead.bone.count; i++) {
		lNodeList[i]->EvaluateGlobalTransform();
		lNodeList[i]->EvaluateGlobalTransform();
		lNodeList[i]->EvaluateGlobalTransform();
	}

	FBXSDK_printf("--- Dash Animations ---\n");

	DashAnimation_t dAnim;
	DashAnimTrack_t dTrack;
	fseek(fp, dHead.anim.offset, SEEK_SET);

	for(i = 0; i < dHead.anim.count; i++) {

		fread(&dAnim, sizeof(DashAnimation_t), 1, fp);
		pos = ftell(fp);

		// Create a new animation
		FbxAnimStack* lAnimStack = FbxAnimStack::Create(pScene, dAnim.name);
		FbxAnimLayer* lAnimLayer = FbxAnimLayer::Create(pScene, "Base Layer");
		lAnimStack->AddMember(lAnimLayer);

		printf("Parsing animation: %s\n", dAnim.name);
		printf("Track offset: 0x%08x\n", dAnim.track_offset);
		printf("Track count: %d\n", dAnim.track_count);
		
		for(k = 0; k < dHead.bone.count; k++) {
			
			printf("Testing Bone: %d\n", k);
			lNodeList[k]->EvaluateGlobalTransform();
			printf("Transform okay\n");
			lNodeList[k]->LclTranslation.GetCurveNode(lAnimLayer, true);
			printf("Pos okay\n");
			lNodeList[k]->LclRotation.GetCurveNode(lAnimLayer, true);
			printf("Rot okay\n");
			lNodeList[k]->LclScaling.GetCurveNode(lAnimLayer, true);
			printf("Scl okay\n");

		}
		
		fseek(fp, dAnim.track_offset, SEEK_SET);
		for(k = 0; k < dAnim.track_count; k++) {

			printf("Reading track %d of %d\n", k, dAnim.track_count);
			fread(&dTrack, sizeof(DashAnimTrack_t), 1, fp);
			printf("Reading values for bone: %d\n", dTrack.bone_id);
			printf("PRS Flag Value: %d\n", dTrack.prs_flags);
			
			int lKeyIndex = 0;
			FbxTime lTime;
			FbxAnimCurve* lCurve;
			int bone_id = (int)dTrack.bone_id;
			lTime.SetSecondDouble(dTrack.time);

			if(dTrack.prs_flags & DASH_ANIMATION_POS) {

				lNodeList[bone_id]->EvaluateGlobalTransform();
				lNodeList[bone_id]->LclTranslation.GetCurveNode(lAnimLayer, true);
				lCurve = lNodeList[bone_id]->LclTranslation.GetCurve(lAnimLayer, FBXSDK_CURVENODE_COMPONENT_X, true);
				lCurve->KeyModifyBegin();
				lKeyIndex = lCurve->KeyAdd(lTime);
				lCurve->KeySetValue(lKeyIndex, dTrack.pos[0]);
				lCurve->KeySetInterpolation(lKeyIndex, FbxAnimCurveDef::eInterpolationLinear);
				lCurve->KeyModifyEnd();

				lCurve = lNodeList[bone_id]->LclTranslation.GetCurve(lAnimLayer, FBXSDK_CURVENODE_COMPONENT_Y, true);
				lCurve->KeyModifyBegin();
				lKeyIndex = lCurve->KeyAdd(lTime);
				lCurve->KeySetValue(lKeyIndex, dTrack.pos[1]);
				lCurve->KeySetInterpolation(lKeyIndex, FbxAnimCurveDef::eInterpolationLinear);
				lCurve->KeyModifyEnd();

				lCurve = lNodeList[bone_id]->LclTranslation.GetCurve(lAnimLayer, FBXSDK_CURVENODE_COMPONENT_Z, true);
				lCurve->KeyModifyBegin();
				lKeyIndex = lCurve->KeyAdd(lTime);
				lCurve->KeySetValue(lKeyIndex, dTrack.pos[2]);
				lCurve->KeySetInterpolation(lKeyIndex, FbxAnimCurveDef::eInterpolationLinear);
				lCurve->KeyModifyEnd();

			}

			if(dTrack.prs_flags & DASH_ANIMATION_ROT) {
				
				printf("rot\n");

				FbxQuaternion fbxQuat;
				fbxQuat.SetAt(0, (double)dTrack.rot[0]);
				fbxQuat.SetAt(1, (double)dTrack.rot[1]);
				fbxQuat.SetAt(2, (double)dTrack.rot[2]);
				fbxQuat.SetAt(3, (double)dTrack.rot[3]);
				FbxVector4 euler = fbxQuat.DecomposeSphericalXYZ();

				lNodeList[bone_id]->LclRotation.GetCurveNode(lAnimLayer, true);
				lCurve = lNodeList[bone_id]->LclRotation.GetCurve(lAnimLayer, FBXSDK_CURVENODE_COMPONENT_X, true);
				lCurve->KeyModifyBegin();
				lKeyIndex = lCurve->KeyAdd(lTime);
				lCurve->KeySetValue(lKeyIndex, euler[0]);
				lCurve->KeySetInterpolation(lKeyIndex, FbxAnimCurveDef::eInterpolationLinear);
				lCurve->KeyModifyEnd();

				lCurve = lNodeList[bone_id]->LclRotation.GetCurve(lAnimLayer, FBXSDK_CURVENODE_COMPONENT_Y, true);
				lCurve->KeyModifyBegin();
				lKeyIndex = lCurve->KeyAdd(lTime);
				lCurve->KeySetValue(lKeyIndex, euler[1]);
				lCurve->KeySetInterpolation(lKeyIndex, FbxAnimCurveDef::eInterpolationLinear);
				lCurve->KeyModifyEnd();

				lCurve = lNodeList[bone_id]->LclRotation.GetCurve(lAnimLayer, FBXSDK_CURVENODE_COMPONENT_Z, true);
				lCurve->KeyModifyBegin();
				lKeyIndex = lCurve->KeyAdd(lTime);
				lCurve->KeySetValue(lKeyIndex, euler[2]);
				lCurve->KeySetInterpolation(lKeyIndex, FbxAnimCurveDef::eInterpolationLinear);
				lCurve->KeyModifyEnd();

			}

			if(dTrack.prs_flags & DASH_ANIMATION_SCL) {

				printf("scl\n");
				lNodeList[bone_id]->LclScaling.GetCurveNode(lAnimLayer, true);
				lCurve = lNodeList[bone_id]->LclScaling.GetCurve(lAnimLayer, FBXSDK_CURVENODE_COMPONENT_X, true);
				lCurve->KeyModifyBegin();
				lKeyIndex = lCurve->KeyAdd(lTime);
				lCurve->KeySetValue(lKeyIndex, dTrack.scl[0]);
				lCurve->KeySetInterpolation(lKeyIndex, FbxAnimCurveDef::eInterpolationLinear);
				lCurve->KeyModifyEnd();

				lCurve = lNodeList[bone_id]->LclScaling.GetCurve(lAnimLayer, FBXSDK_CURVENODE_COMPONENT_Y, true);
				lCurve->KeyModifyBegin();
				lKeyIndex = lCurve->KeyAdd(lTime);
				lCurve->KeySetValue(lKeyIndex, dTrack.scl[1]);
				lCurve->KeySetInterpolation(lKeyIndex, FbxAnimCurveDef::eInterpolationLinear);
				lCurve->KeyModifyEnd();

				lCurve = lNodeList[bone_id]->LclScaling.GetCurve(lAnimLayer, FBXSDK_CURVENODE_COMPONENT_Z, true);
				lCurve->KeyModifyBegin();
				lKeyIndex = lCurve->KeyAdd(lTime);
				lCurve->KeySetValue(lKeyIndex, dTrack.scl[2]);
				lCurve->KeySetInterpolation(lKeyIndex, FbxAnimCurveDef::eInterpolationLinear);
				lCurve->KeyModifyEnd();

			}

		}

		fseek(fp, pos, SEEK_SET);

	}

	FbxGeometry* lMeshAttribute = (FbxGeometry*) pMesh->GetNodeAttribute();
	FbxSkin* lSkin = FbxSkin::Create(pScene, "");
	pPose->Add(lNode, lNode->EvaluateGlobalTransform());
	for(i = 0; i < dHead.bone.count; i++) {
		lSkin->AddCluster(lClusterList[i]);
	}
	pScene->AddPose(pPose);
	lMeshAttribute->AddDeformer(lSkin);

	fclose(fp);
	free(lClusterList);
	free(lNodeList);

	return lNode;

}