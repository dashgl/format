# Install Linux SDK

First step is that we need to install the Autodesk FBX SDK on Linux.
Currently I am using ElementaryOs version 5.4, which is based on 
Ubuntu 18.04. In general we need to look into install instructions
for Ubuntu 20.04, and Debian 10.4 Buster. We expect the environment
to be a x86_64 desktop environment. 

```
$ sudo apt-get install cmake build-essential zlib1g-dev libxml2-dev
```

## Download

The download page for the Autodesk DSK can be found here: https://www.autodesk.com/developer-network/platform-technologies/fbx-sdk-2020-0  
And the link for the fbx installer specifically can be found at: https://www.autodesk.com/content/dam/autodesk/www/adn/fbx/2020-0-1/fbx202001_fbxsdk_linux.tar.gz  

```
$ cd Documents
$ mkdir fbx_sdk && cd fbx_sdk
$ wget https://www.autodesk.com/content/dam/autodesk/www/adn/fbx/2020-0-1/fbx202001_fbxsdk_linux.tar.gz
$ tar -xzf fbx202001_fbxsdk_linux.tar.gz
$ rm fbx202001_fbxsdk_linux.tar.gz
$ ./fbx202001_fbxsdk_linux
File(s) will be extracted to: .
Please confirm [y/n] ? (y)

To continue installing the software, you must agree
to the terms of the software license agreement.

Type "yes" to agree and continue the installation. Agree [yes/no] ? (yes)
$ rm fbx202001_fbxsdk_linux
```

Once we have the files installed, next step is to compile and run the examples.

```
$ cd samples/ExportScene01
$ cmake .
$ make
$ cd ../bin/x64/gcc-static/debug
$ ./ExportScene01
```

You should have a ExportScene01.fbx in the directory
