/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019,2020 Benjamin Collins (kion @ dashgl.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/

#ifndef _DASH_H
#define _DASH_H

#include <stdint.h>

#define DASH_MAGIC_NUMBER 		0x48534144
#define DASH_MAJOR_VERSION 			  0x01
#define DASH_MINOR_VERSION 			  0x05

#define DASH_LABEL_TEX 			0x00786574
#define DASH_LABEL_MAT 			0x0074616D
#define DASH_LABEL_VERT 		0x74726576
#define DASH_LABEL_FACE 		0x65636166
#define DASH_LABEL_BONE 		0x656E6F62
#define DASH_LABEL_ANIM 		0x6D696E61

// Render Side

#define DASH_MATERIAL_FRONTSIDE		   	0
#define DASH_MATERIAL_BACKSIDE			1
#define DASH_MATERIAL_DOUBLESIDE		2

// Shader Types

#define DASH_MATERIAL_BASIC			   	0
#define DASH_MATERIAL_LAMBERT			1
#define DASH_MATERIAL_PHONG			   	2

// Blend Equations

#define DASH_MATERIAL_NORMAL			0
#define DASH_MATERIAL_ADDITIVE			1
#define DASH_MATERIAL_SUBTRACT			2
#define DASH_MATERIAL_MULTIPLY			3

// Blend Contansts

#define DASH_MATERIAL_ZERO					0
#define DASH_MATERIAL_ONE				 	1
#define DASH_MATERIAL_SRC_COLOR		   		2
#define DASH_MATERIAL_ONE_MINUS_SRC_COLOR 	3
#define DASH_MATERIAL_SRC_ALPHA		   		4
#define DASH_MATERIAL_ONE_MINUS_SRC_ALPHA 	5
#define DASH_MATERIAL_DST_ALPHA		   		6
#define DASH_MATERIAL_ONE_MINUS_DST_ALPHA 	7
#define DASH_MATERIAL_DST_COLOR		   		8
#define DASH_MATERIAL_ONE_MINUS_DST_COLOR 	9
#define DASH_MATERIAL_SRC_ALPHA_SATURATE  	10

// Animation PRS FLags

#define DASH_ANIMATION_POS 1
#define DASH_ANIMATION_ROT 2
#define DASH_ANIMATION_SCL 4

typedef struct {
	uint32_t magic;
	uint16_t major_version;
	uint16_t minor_version;
	uint32_t header_offset;
	uint32_t header_count;
} DashMagic_t;

typedef struct {
	uint32_t label;
	uint32_t flags;
	uint32_t offset;
	uint32_t count;
} DashProperty_t;

typedef struct {
	DashProperty_t tex;
	DashProperty_t mat;
	DashProperty_t vert;
	DashProperty_t face;
	DashProperty_t bone;
	DashProperty_t anim;
} DashHeader_t;

typedef struct {
	char name[0x20];
	int16_t tex_id;
	uint16_t flip_y;
	uint32_t img_offset;
	uint32_t img_length;
	uint16_t img_width;
	uint16_t img_height;
	uint16_t wrap_s;
	uint16_t wrap_t;
	uint8_t nop[0x0c];
} DashTexture_t;

typedef struct {
	char name[0x20];
	int16_t mat_id;
	int16_t tex_id;
	uint16_t use_blending;
	uint16_t blend_equation;
	uint16_t blend_src;
	uint16_t blend_dst;
	uint16_t skinning;
	uint16_t shader_type;
	uint16_t face_side;
	uint16_t shadow_side;
	uint16_t ignore_lights;
	uint16_t vertex_color;
	uint16_t use_alpha;
	uint16_t skip_render;
	float alpha_test;
	float diffuse[4];
	float emissive[4];
	float specular[4];
} DashMaterial_t;

typedef struct {
	float pos[3];
	uint16_t skin_index[4];
	float skin_weight[4];
} DashVertex_t;

typedef struct {
	uint16_t mat_id;
	uint16_t nop;
	uint32_t a;
	uint32_t b;
	uint32_t c;
	uint8_t aClr[4];
	uint8_t bClr[4];
	uint8_t cClr[4];
	float aNrm[3];
	float bNrm[3];
	float cNrm[3];
	float aUv[2];
	float bUv[2];
	float cUv[2];
} DashFace_t;

typedef struct {
	char name[0x20];
	int16_t bone_id;
	int16_t parent_id;
	uint8_t nop[0x0c];
	float matrix[0x10];
} DashBone_t;

typedef struct {
	char name[0x20];
	int16_t anim_id;
	uint16_t fps;
	uint32_t track_offset;
	uint32_t track_count;
	float duration;
} DashAnimation_t;

typedef struct {
	int16_t bone_id;
	int16_t prs_flags;
	float time;
	float pos[3];
	float rot[4];
	float scl[3];
} DashAnimTrack_t;

#endif