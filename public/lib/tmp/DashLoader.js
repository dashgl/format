/******************************************************************************
 *
 * MIT License
 *
 * Copyright (c) 2019 Benjamin Collins (kion @ dashgl.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 *****************************************************************************/

"use strict";

THREE.DashLoader = function() {

	this.textures = [];
	this.materials = [];
	this.bones = [];
	this.geometry = new THREE.Geometry();
	this.animations = [];

}

THREE.DashLoader.prototype = {

	constructor: THREE.DashLoader,

	load : function(url, callback) {

		let ajax = new XMLHttpRequest();
		ajax.open("GET", url);
		ajax.responseType = "arraybuffer";

		ajax.onreadystatechange = () => {
			if(ajax.readyState !== 4) {
				return;
			}

			if(ajax.status !== 200){ 
				return callback("Could not load: " + url);
			}

			let buffer = ajax.response;
			let mesh;

			try {
				mesh = this.parse(buffer);
			} catch(err) {
				
			}

			callback(null, mesh);
		}
		
		ajax.send();


	},

	loadAsync : function(url) {

		return new Promise( (resolve, reject) => {
			
			this.load(url, (err, mesh) => {
				
				if(err) {
					return reject(err);
				}

				resolve(mesh);

			});

		});

	},

	parse : function(byteArray) {

		// Check Magic number and version

		const DASH_MAGIC_NUMBER  = 0x48534144;
		const DASH_MAJOR_VERSION = 0x01;
		const DASH_MINOR_VERSION = 0x05;

		this.view = new DataView(byteArray);
		let magic = this.view.getUint32(0x00, true);
		let version = this.view.getUint16(0x04, true);
		let sub_version = this.view.getUint16(0x06, true);
		
		if(magic !== DASH_MAGIC_NUMBER) {
			throw new Error("Invalid magic number: 0x", magic.toString(16));
		}

		if(version !== DASH_MAJOR_VERSION) {
			throw new Error("Invalid version number: " + version);
		}

		if(sub_version !== DASH_MINOR_VERSION) {
			throw new Error("Invalid subversion number: " + sub_version);
		}

		// Parse Header

		this.header = {
			textures : {
				offset : this.view.getUint32(0x18, true),
				count : this.view.getUint32(0x1c, true)
			},
			materials : {
				offset : this.view.getUint32(0x28, true),
				count : this.view.getUint32(0x2c, true)
			},
			vertices : {
				offset : this.view.getUint32(0x38, true),
				count : this.view.getUint32(0x3c, true)
			},
			faces : {
				offset : this.view.getUint32(0x48, true),
				count : this.view.getUint32(0x4c, true)
			},
			bones : {
				offset : this.view.getUint32(0x58, true),
				count : this.view.getUint32(0x5c, true)
			},
			animations : {
				offset : this.view.getUint32(0x68, true),
				count : this.view.getUint32(0x6c, true)
			}

		}

		// Read Properties (Materials)

		this.readTextures();
		this.readMaterials();

		// Read Properties (Geometry)
		
		this.readVertices();
		this.readFaces();
		
		// Read Properties (Skeleton)
		
		this.readBones();
		this.readAnimations();

		// Create buffer geometry 

		let buffer = new THREE.BufferGeometry();
		buffer.fromGeometry(this.geometry);
		buffer.animations = this.animations;

		let mesh;
		if(!this.armSkeleton) {
			buffer.removeAttribute("skinIndex");
			buffer.removeAttribute("skinWeight");
			mesh = new THREE.Mesh(buffer, this.materials);
		} else {
			mesh = new THREE.SkinnedMesh(buffer, this.materials);
			let rootBone = this.armSkeleton.bones[0];
			mesh.add(rootBone);
			mesh.bind(this.armSkeleton);
		}

		console.log(buffer);
		console.log(this.materials);

		return mesh;

	},

	readTextures : function() {

		if(this.header.textures.offset === 0) {
			return;
		}
	
		let ofs = this.header.textures.offset;
		for(let i = 0; i < this.header.textures.count; i++) {
			
			let name = "";
			for(let k = 0; k < 0x20; k++) {
				let byte = this.view.getUint8(ofs + k);
				if(!byte) {
					break;
				}
				name += String.fromCharCode(byte);
			}

			let img = {
				id : this.view.getInt16(ofs + 0x20, true),
				flipY : this.view.getUint16(ofs + 0x22, true),
				offset : this.view.getUint32(ofs + 0x24, true),
				length : this.view.getUint32(ofs + 0x28, true),
				width : this.view.getUint16(ofs + 0x2c, true),
				height : this.view.getUint16(ofs + 0x2e, true),
				wrapS : this.view.getUint16(ofs + 0x30, true),
				wrapT : this.view.getUint16(ofs + 0x32, true)
			}

			let base64 = "";

			for(let k = 0; k < img.length; k++) {
				let ch = this.view.getUint8(img.offset + k);
				base64 += String.fromCharCode(ch);
			}
			
			let image = new Image();
			image.src = "data:image/png;base64," + btoa(base64);

			let texture = new THREE.Texture(image);
			texture.wrapS = img.wrapS + 1000;
			texture.wrapT = img.wrapT + 1000;
			texture.flipY = img.flipY;
			texture.name = name;
			image.onload = () => {
				texture.needsUpdate = true;
			}

			this.textures.push(texture);
			ofs += 0x40;

		}
		

	},

	readMaterials : function() {
		
		if(!this.header.materials.offset) {
			return;
		}

		const DASH_MATERIAL_BASIC			   = 0;
		const DASH_MATERIAL_LAMBERT			 = 1;
		const DASH_MATERIAL_PHONG			   = 2;

		let ofs = this.header.materials.offset;
		for(let i = 0; i < this.header.materials.count; i++) {
			
			let name = "";
			for(let k = 0; k < 0x20; k++) {
				let byte = this.view.getUint8(ofs + k);
				if(!byte) {
					break;
				}
				name += String.fromCharCode(byte);
			}
			
			let mat = {
				id : this.view.getInt16(ofs + 0x20, true),
				textureId : this.view.getInt16(ofs + 0x22, true),
				blending : {
					use : this.view.getUint16(ofs + 0x24, true),
					equation : this.view.getUint16(ofs + 0x26, true),
					src : this.view.getUint16(ofs + 0x28, true),
					dst : this.view.getUint16(ofs + 0x2a, true)
				},
				skinning : this.view.getUint16(ofs + 0x2c, true),
				shader_type : this.view.getUint16(ofs + 0x2e, true),

				render_side : this.view.getUint16(ofs + 0x30, true),
				shadow_side : this.view.getUint16(ofs + 0x32, true),

				ignore_light : this.view.getUint16(ofs + 0x34, true),
				vertex_color : this.view.getUint16(ofs + 0x36, true),

				use_alpha : this.view.getUint16(ofs + 0x38, true),
				skip_render : this.view.getUint16(ofs + 0x3a, true),

				alphaTest : this.view.getFloat32(ofs + 0x3c, true),
				diffuse : [
					this.view.getFloat32(ofs + 0x40, true),
					this.view.getFloat32(ofs + 0x44, true),
					this.view.getFloat32(ofs + 0x48, true),
					this.view.getFloat32(ofs + 0x4c, true)
				],
				emissive : [
					this.view.getFloat32(ofs + 0x50, true),
					this.view.getFloat32(ofs + 0x54, true),
					this.view.getFloat32(ofs + 0x58, true),
					this.view.getFloat32(ofs + 0x5c, true)
				],
				specular : [
					this.view.getFloat32(ofs + 0x60, true),
					this.view.getFloat32(ofs + 0x64, true),
					this.view.getFloat32(ofs + 0x68, true),
					this.view.getFloat32(ofs + 0x6c, true)
				]
			};
			
			console.log(mat);

			// Create Material

			let material;
			
			switch(mat.shader_type) {
			case DASH_MATERIAL_BASIC:
				material = new THREE.MeshBasicMaterial();
				break;
			case DASH_MATERIAL_LAMBERT:
				material = new THREE.MeshLambertMaterial();
				break;
			case DASH_MATERIAL_PHONG:
				material = new THREE.MeshPhongMaterial();
				break;
			default:
				material = new THREE.MeshBasicMaterial();
				break;
			}
			
			material = new THREE.MeshBasicMaterial();
			material.name = name;

			if(mat.textureId !== -1) {
				material.map = this.textures[mat.textureId];
			}
			
			// Blending
			if(mat.blending.use) {
				material.blending = THREE.CustomBlending;
				material.blendEquation = mat.blending.equation;
				material.blendSrc = mat.blending.src;
				material.blendDst = mat.blending.dst;
			}

			// Attributes
			
			material.skinning = mat.skinning;
			material.side = mat.render_side;
			material.shadowSide = mat.shadow_side;
			if(mat.ignore_light) {
				material.lights = false;
			}
			//material.vertexColors = mat.vertex_color;

			if(mat.use_alpha) {
				material.transparent = true;
				console.log("use_alpha");
			}
			
			if(mat.skip_render) {
				material.visible = false;
				console.log("skipping");
			}
			material.alphaTest = mat.alphaTest;

			// Diffuse
			
			/*
			console.log(mat.diffuse);
			material.color.setRGB(mat.diffuse[0], mat.diffuse[1], mat.diffuse[2]);
			//material.opacity = mat.diffuse[3];
				
			// Emissive
			
			if(material.emissive) {
				material.emissive.setRGB(mat.emissive[0], mat.emissive[1], mat.emissive[2]);
				mat.emissiveIntensity = mat.emissive[3];
			}

			if(material.specular) {
				material.specular.setRGB(mat.specular[0], mat.specular[1], mat.specular[2]);
				material.shininess = mat.specular[3];
			}
			*/

			material.needsUpdate = true;
			this.materials.push(material);
			ofs += 0x70;
			
			console.log(material);

		}

	},

	readVertices : function() {
		
		if(!this.header.vertices.offset) {
			return;
		}
				
		let ofs = this.header.vertices.offset;
		for(let i = 0; i < this.header.vertices.count; i++) {
			
			let pos = {
				x : this.view.getFloat32(ofs + 0x00, true),
				y : this.view.getFloat32(ofs + 0x04, true),
				z : this.view.getFloat32(ofs + 0x08, true)
			};
			
			let idx = {
				x : this.view.getUint16(ofs + 0x0c, true),
				y : this.view.getUint16(ofs + 0x0e, true),
				z : this.view.getUint16(ofs + 0x10, true),
				w : this.view.getUint16(ofs + 0x12, true)
			};
			
			let wgt = {
				x : this.view.getFloat32(ofs + 0x14, true),
				y : this.view.getFloat32(ofs + 0x18, true),
				z : this.view.getFloat32(ofs + 0x1c, true),
				w : this.view.getFloat32(ofs + 0x20, true)
			};
			
			ofs += 0x24;
			
			let vertex = new THREE.Vector3(pos.x, pos.y, pos.z);
			let skinIndex = new THREE.Vector4(idx.x, idx.y, idx.z, idx.w);
			let skinWeight = new THREE.Vector4(wgt.x, wgt.y, wgt.z, wgt.w);
			
			this.geometry.vertices.push(vertex);
			this.geometry.skinIndices.push(skinIndex);
			this.geometry.skinWeights.push(skinWeight);
			
		}

	},

	readFaces : function() {
		
		if(!this.header.faces.offset || !this.header.faces.count) {
			return;
		}
		
		let ofs = this.header.faces.offset;
		for(let i = 0; i < this.header.faces.count; i++) {
			
			let matId = this.view.getInt16(ofs + 0x00, true);
			
			let a = this.view.getUint32(ofs + 0x04, true);
			let b = this.view.getUint32(ofs + 0x08, true);
			let c = this.view.getUint32(ofs + 0x0c, true);

			let aClr = new THREE.Color(
				this.view.getUint8(ofs + 0x10) / 255,
				this.view.getUint8(ofs + 0x11) / 255,
				this.view.getUint8(ofs + 0x12) / 255
			);

			let aAlpha = this.view.getUint8(ofs + 0x13);
			
			let bClr = new THREE.Color(
				this.view.getUint8(ofs + 0x14) / 255,
				this.view.getUint8(ofs + 0x15) / 255,
				this.view.getUint8(ofs + 0x16) / 255
			);

			let bAlpha = this.view.getUint8(ofs + 0x17);

			let cClr = new THREE.Color(
				this.view.getUint8(ofs + 0x18) / 255,
				this.view.getUint8(ofs + 0x19) / 255,
				this.view.getUint8(ofs + 0x1a) / 255
			);

			let cAlpha = this.view.getUint8(ofs + 0x1b);
			
			let aNrm = new THREE.Vector3(
				this.view.getFloat32(ofs + 0x1c, true),
				this.view.getFloat32(ofs + 0x20, true),
				this.view.getFloat32(ofs + 0x24, true)
			)
			
			let bNrm = new THREE.Vector3(
				this.view.getFloat32(ofs + 0x28, true),
				this.view.getFloat32(ofs + 0x2c, true),
				this.view.getFloat32(ofs + 0x30, true)
			);
			
			let cNrm = new THREE.Vector3(
				this.view.getFloat32(ofs + 0x34, true),
				this.view.getFloat32(ofs + 0x38, true),
				this.view.getFloat32(ofs + 0x3c, true)
			);

			let aUv = new THREE.Vector2(
				this.view.getFloat32(ofs + 0x40, true),
				this.view.getFloat32(ofs + 0x44, true)
			);

			let bUv = new THREE.Vector2(
				this.view.getFloat32(ofs + 0x48, true),
				this.view.getFloat32(ofs + 0x4c, true)
			);

			let cUv = new THREE.Vector2(
				this.view.getFloat32(ofs + 0x50, true),
				this.view.getFloat32(ofs + 0x54, true)
			);

			// Create Face
			
			let face = new THREE.Face3(a, b, c);
			face.materialIndex = matId;
			
			face.vertexNormals = [ aNrm, bNrm, cNrm ];
			face.vertexColors = [ aClr, bClr, cClr ];

			this.geometry.faces.push(face);
			this.geometry.faceVertexUvs[0].push([aUv, bUv, cUv]);
			
			ofs += 0x58;

		}

	},

	readBones : function() {
		
		if(!this.header.bones.offset) {
			return;
		}
		
		let ofs = this.header.bones.offset;
		for(let i = 0; i < this.header.bones.count; i++) {
			
			let name = "";
			for(let k = 0; k < 0x20; k++) {
				let byte = this.view.getUint8(ofs + k);
				if(!byte) {
					break;
				}
				name += String.fromCharCode(byte);
			}
			
			let id =  this.view.getInt16(ofs + 0x20, true);
			let parentId =  this.view.getInt16(ofs + 0x22, true);

			console.log("%d %d", id, parentId);
	
			let matrix = new THREE.Matrix4();
			matrix.elements[0] = this.view.getFloat32(ofs + 0x30, true);
			matrix.elements[1] = this.view.getFloat32(ofs + 0x34, true);
			matrix.elements[2] = this.view.getFloat32(ofs + 0x38, true);
			matrix.elements[3] = this.view.getFloat32(ofs + 0x3c, true);
			matrix.elements[4] = this.view.getFloat32(ofs + 0x40, true);
			matrix.elements[5] = this.view.getFloat32(ofs + 0x44, true);
			matrix.elements[6] = this.view.getFloat32(ofs + 0x48, true);
			matrix.elements[7] = this.view.getFloat32(ofs + 0x4c, true);
			matrix.elements[8] = this.view.getFloat32(ofs + 0x50, true);
			matrix.elements[9] = this.view.getFloat32(ofs + 0x54, true);
			matrix.elements[10] = this.view.getFloat32(ofs + 0x58, true);
			matrix.elements[11] = this.view.getFloat32(ofs + 0x5c, true);
			matrix.elements[12] = this.view.getFloat32(ofs + 0x60, true);
			matrix.elements[13] = this.view.getFloat32(ofs + 0x64, true);
			matrix.elements[14] = this.view.getFloat32(ofs + 0x68, true);
			matrix.elements[15] = this.view.getFloat32(ofs + 0x6c, true);

			ofs += 0x70;
			
			let bone = new THREE.Bone();
			bone.name = name;

			if(this.bones[parentId]) {
				this.bones[parentId].add(bone);
			}
			bone.applyMatrix(matrix);
			bone.updateMatrix();
			bone.updateMatrixWorld();
			this.bones.push(bone);
		}
		
		this.armSkeleton = new THREE.Skeleton(this.bones);

	},

	readAnimations : function() {

		if(!this.header.animations.offset) {
			return;
		}

		const DASH_ANIMATION_POS			 = 0x01;
		const DASH_ANIMATION_ROT			 = 0x02;
		const DASH_ANIMATION_SCL			 = 0x04;

		let ofs = this.header.animations.offset;
		
		for(let i = 0; i < this.header.animations.count; i++) {
			
			let name = "";
			for(let k = 0; k < 0x20; k++) {
				let byte = this.view.getUint8(ofs + k);
				if(!byte) {
					break;
				}
				name += String.fromCharCode(byte);
			}
			
			let id = this.view.getInt16(ofs + 0x20, true);
			let fps = this.view.getInt16(ofs + 0x22, true);
			let offset = this.view.getUint32(ofs + 0x24, true);
			let count = this.view.getUint32(ofs + 0x28, true);
			let duration = this.view.getFloat32(ofs + 0x2c, true);
			
			let animation = {
				name : name,
				fps : fps,
				length : duration,
				hierarchy : []
			};
			ofs += 0x30;

			let pointer;
			for(let k = 0; k < count; k++) {
				
				let track = {
					bone : this.view.getInt16(offset + 0x00, true),
					flags : this.view.getUint16(offset + 0x02, true),
					time : this.view.getFloat32(offset + 0x04, true),
					pos : [
						this.view.getFloat32(offset + 0x08, true),
						this.view.getFloat32(offset + 0x0c, true),
						this.view.getFloat32(offset + 0x10, true)
					],
					rot : [
						this.view.getFloat32(offset + 0x14, true),
						this.view.getFloat32(offset + 0x18, true),
						this.view.getFloat32(offset + 0x1c, true),
						this.view.getFloat32(offset + 0x20, true)
					],
					scl : [
						this.view.getFloat32(offset + 0x24, true),
						this.view.getFloat32(offset + 0x28, true),
						this.view.getFloat32(offset + 0x2c, true)
					]
				};
				offset += 0x30;
				
				if(!animation.hierarchy[track.bone]) {
					animation.hierarchy.push({
						parent : track.bone - 1,
						keys : []
					});
				}
				
				let key = {
					time : track.time
				};

				if(track.flags & DASH_ANIMATION_POS) {
					key.pos = track.pos;
				}

				if(track.flags & DASH_ANIMATION_ROT) {
					key.rot = track.rot;
				}

				if(track.flags & DASH_ANIMATION_SCL) {
					key.scl = track.scl;
				}

				animation.hierarchy[track.bone].keys.push(key);

			}
			
			let clip = THREE.AnimationClip.parseAnimation(animation, this.bones);
			if(!clip) {
				continue;
			}

			clip.optimize();
			this.animations.push(clip);
			
		}

	}


}

