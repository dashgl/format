rm ColladaExporter.js
rm DashExporter.js
rm DashLoader.js
rm GLTFExporter.js
rm GLTFLoader.js
rm OrbitControls.js
rm three.min.js

wget https://gitlab.com/dashgl/format/raw/master/src/threejs/DashExporter.js
wget https://gitlab.com/dashgl/format/raw/master/src/threejs/DashLoader.js
wget https://raw.githubusercontent.com/mrdoob/three.js/dev/build/three.min.js
wget https://raw.githubusercontent.com/mrdoob/three.js/dev/examples/js/controls/OrbitControls.js
wget https://raw.githubusercontent.com/mrdoob/three.js/dev/examples/js/exporters/ColladaExporter.js
wget https://raw.githubusercontent.com/mrdoob/three.js/dev/examples/js/loaders/GLTFLoader.js
wget https://raw.githubusercontent.com/mrdoob/three.js/dev/examples/js/exporters/GLTFExporter.js
