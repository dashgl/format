/*

const section = document.getElementById("section");
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera();
const renderer = new THREE.WebGLRenderer();
const controls = new THREE.OrbitControls( camera, section );

section.appendChild(renderer.domElement);

resize();
animate();

window.addEventListener("resize", resize);

function resize() {

	let width = section.offsetWidth;
	let height = section.offsetHeight;

	camera.aspect = width / height;
	camera.updateProjectionMatrix();
	renderer.setSize( width, height);

}

function animate() {

	requestAnimationFrame(animate);
	controls.update();
	renderer.render(scene, camera);

}
*/

"use strict";

var camera, controls, scene, renderer;
const clock = new THREE.Clock();
const section = document.getElementById("section");
const select = document.getElementById("anim_select");
const MEM = {};

init();
animate();

function init() {

    scene = new THREE.Scene();
    scene.background = new THREE.Color(0xcccccc);
    //scene.fog = new THREE.FogExp2(0xcccccc, 0.002);

	let width = section.offsetWidth;
	let height = section.offsetHeight;

    renderer = new THREE.WebGLRenderer({
        antialias: true
    });

    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    section.appendChild(renderer.domElement);

    camera = new THREE.PerspectiveCamera(60, width / height, 1, 1000);
    camera.position.set(50, 40, 0);

    // controls

    controls = new THREE.OrbitControls(camera, renderer.domElement);

    controls.enableDamping = true;
    controls.dampingFactor = 0.25;

    controls.screenSpacePanning = false;

    controls.minDistance = 1;
    controls.maxDistance = 1500;

    controls.maxPolarAngle = Math.PI / 2;


    // lights

	scene.add(new THREE.GridHelper(100, 10));
	scene.add(new THREE.AmbientLight(0xffffff));

	// resize

    window.addEventListener('resize', onWindowResize, false);

	document.body.addEventListener('dragover', handleDragOver, false);
	document.body.addEventListener('drop', handleFileSelect, false);
	select.addEventListener("change", handleAnimChange);

}

function resetScene() {
		
	if(MEM.mesh) {
		scene.remove(MEM.mesh);
	}

	if(MEM.helper) {
		scene.remove(MEM.helper);
	}

}

const gltfImport = document.getElementById("import.gltf");
const gltfFile = document.getElementById("file.gltf");
gltfImport.addEventListener("click", function() {
	gltfFile.click();
});

gltfFile.addEventListener("change", function(evt) {
	evt.preventDefault();

	var files = evt.target.files;
	let file = files[0];

	let reader = new FileReader();

	reader.onload = function() {

		let loader = new THREE.GLTFLoader();
		loader.parse(reader.result, "/", function(gltf) {
			
			gltf.scene.traverse( function ( child ) {
				if(!child.isMesh) {
					return;
				}
				scene.add(child);
			})

			console.log(result);

		});
		/*
		scene.add(mesh);

		let helper = new THREE.SkeletonHelper(mesh);
		helper.material.linewidth = 1;
		scene.add(helper);

		let anims = mesh.geometry.animations;
		select.innerHTML = "";

		let option = document.createElement("option");
		option.textContent = "Select Animation";
		option.setAttribute("value", -1);
		select.appendChild(option);

		for(let i = 0; i < anims.length; i++) {

			option = document.createElement("option");
			option.textContent = anims[i].name
			option.setAttribute("value", i);
			select.appendChild(option);

		}
		
		MEM.mesh = mesh;
		MEM.helper = helper;
		MEM.mixer = new THREE.AnimationMixer(mesh);
		*/
	}

	reader.readAsArrayBuffer(file)

});

function onWindowResize() {

	let width = section.offsetWidth;
	let height = section.offsetHeight;

    camera.aspect = width / height;
    camera.updateProjectionMatrix();

    renderer.setSize(width, height);

}

function animate() {

    requestAnimationFrame(animate);

    controls.update();

	if(MEM.mixer && MEM.action) {

		let delta = clock.getDelta();
		MEM.mixer.update(delta);


	}

    render();

}

function render() {

    renderer.render(scene, camera);

}

function handleAnimChange(evt) {

	console.log(this.value);

	if(MEM.action) {
		MEM.action.stop();
	}

	let index = parseInt(this.value);
	if(index === -1) {
		return;
	}

	let clip = MEM.mesh.geometry.animations[index];
	MEM.action = MEM.mixer.clipAction(clip, MEM.mesh);
	MEM.action.play();

}

function handleFileSelect(evt) {
	evt.stopPropagation();
	evt.preventDefault();

	var files = evt.dataTransfer.files;
	let file = files[0];

	let reader = new FileReader();

	reader.onload = function() {
		
		if(MEM.mesh) {
			scene.remove(MEM.mesh);
		}

		if(MEM.helper) {
			scene.remove(MEM.helper);
		}

		let loader = new THREE.DashLoader();
		let mesh = loader.parse(reader.result);
		scene.add(mesh);

		let helper = new THREE.SkeletonHelper(mesh);
		helper.material.linewidth = 1;
		scene.add(helper);

		let anims = mesh.geometry.animations;
		select.innerHTML = "";

		let option = document.createElement("option");
		option.textContent = "Select Animation";
		option.setAttribute("value", -1);
		select.appendChild(option);

		for(let i = 0; i < anims.length; i++) {

			option = document.createElement("option");
			option.textContent = anims[i].name
			option.setAttribute("value", i);
			select.appendChild(option);

		}
		
		MEM.mesh = mesh;
		MEM.helper = helper;
		MEM.mixer = new THREE.AnimationMixer(mesh);

	}

	reader.readAsArrayBuffer(file)

}

function handleDragOver(evt) {

	evt.stopPropagation();
	evt.preventDefault();
	evt.dataTransfer.dropEffect = 'copy';

}

const fileObj = document.getElementById('file.obj');
fileObj.addEventListener('change', async function(evt) {

	console.log(evt.target.files);

	for(let i = 0; i < evt.target.files.length; i++) {
		
		let file = evt.target.files[i];
		let objText = await readFileAsText(file);
		let loader = new THREE.OBJLoader();
		let groups = loader.parse(objText);
		let mesh = groups.children[0];
		
		scene.add(mesh);
		exportRawData(mesh.geometry);

	}
	
});

function exportRawData(geometry) {

	console.log("exporting data!!!");
	console.log(geometry);

	const COUNT = geometry.attributes.position.count;

	let buffer = new ArrayBuffer(COUNT * 20);
	let view = new DataView(buffer);
	let offset = 0;

	for(let i = 0; i < COUNT; i++) {
		
		let x = geometry.attributes.position.array[i * 3 + 0];
		let y = geometry.attributes.position.array[i * 3 + 1];
		let z = geometry.attributes.position.array[i * 3 + 2];
		let u = geometry.attributes.position.array[i * 2 + 0];
		let v = geometry.attributes.position.array[i * 2 + 1];

		console.log(x, y, z);
	
		view.setFloat32(offset + 0, x);
		view.setFloat32(offset + 4, y);
		view.setFloat32(offset + 8, z);
		view.setFloat32(offset + 12, u);
		view.setFloat32(offset + 16, v);
		offset += 20;

	}

	let blob = new Blob([buffer]);
	saveAs(blob, "shark.bin");

}

function readFileAsText(file) {

	return new Promise( (resolve, reject) => {

		let reader = new FileReader();

		reader.onload = function(evt) {

			resolve(evt.target.result);

		}

		reader.readAsText(file);

	});

}
